import asyncio
from datetime import datetime, timedelta
import random
import time
from typing import Generator, Tuple
import uuid

from app.kafka_topics.builders import carrier_profile, shipment, shipper, shipper_price, planning_transport_plan, stop_timestamp, transport_offer_match, user, facility_profile, operators, shipment_draft, transport_offer, tour_execution, transport_plan
from app.kafka_topics.models.shipment_draft import Shipper
from app.libs.kafka import KafkaMessage, GenericKafkaProducer


def generate() -> Generator[Tuple[str, KafkaMessage], None, None]:
    users = []
    for name in ["operator1", "operator2"]:
        topic, msg = user.build_message(name=name, email=f"{name}@example.com")
        users.append(str(msg.key["user_id"]))
        yield topic, msg
    
    facilities = []
    for _ in range(2):
        topic, msg = facility_profile.build_message()
        facilities.append(msg.key["entity_id"])
        yield topic, msg
    
    shippers = []
    for _ in range(2):
        topic, msg = shipper.build_message(mothership_id=random.randint(0, 10000))
        shippers.append(msg)
        yield topic, msg

    shipments = []
    for x in range(2):
        topic, msg = shipment.build_message(
            shipper=Shipper.fake(shipper_id=shippers[x].value["data"]["mothership_id"]),
            loads=[{
                "load_requirements": {
                    "loading_requirements": [{
                        "reference": f"ref{x}-load",
                        "note": "",
                        "loading_window_open_at": datetime.now(),
                        "loading_window_closed_at": datetime.now() + timedelta(hours=12),
                        "facility_profile_id": facilities[0],
                        "warehouse_address_id": 0
                    }],
                    "unloading_requirements": [{
                        "reference": f"ref{x}-unload",
                        "note": "",
                        "unloading_window_open_at": datetime.now() + timedelta(days=5),
                        "unloading_window_closed_at": datetime.now() + timedelta(days=5, hours=12),
                        "facility_profile_id": facilities[1],
                        "warehouse_address_id": 0
                    }],
                }
            }]
        )
        yield topic, msg
        shipments.append(str(msg.key["entity_id"]))
    
    carriers = []
    for _ in range(2):
        topic, msg = carrier_profile.build_message()
        yield topic, msg
        carriers.append(str(msg.key["entity_id"]))


    for shipment_id in shipments:
        topic, msg = operators.build_message(
            entity_id=shipment_id, operator_ids=users, roles=["snr_aom", "aom"]
        )
        yield topic, msg

        topic, msg = transport_offer.build_message(
            shipment_ids=[shipment_id]
        )
        yield topic, msg

        transport_offer_id = str(msg.key["entity_id"])

        topic, msg = transport_offer_match.build_message(
            transportoffer={"id": transport_offer_id, "version": "1"},
            carrier={"id": carriers[0], "contact_id":carriers[1]},
        )
        yield topic, msg

    stop_ids = []
    for shipment_id in shipments:
        topic, msg = planning_transport_plan.build_message(
            facility_ids=facilities,
            shipment_id=shipment_id
        )
        yield topic, msg

        # transport_plan_id = msg.key["entity_id"]
        # stop_ids.extend(
        #     [s["stop_id"] for s in msg.value["data"]["entity"]["stops"]]
        # )
        # topic, msg = tour_execution.build_message(
        #     shipment_id=shipment_id,
        #     transport_plan_id=transport_plan_id,
        #     tenant="sennder",
        # )
        # yield topic, msg
        # print(msg.key["entity_id"])

        # topic, msg = shipper_price.build_message(
        #     shipment_id=shipment_id
        # )
        # yield topic, msg

    # input("Waiting for stop timestamps...")

    # for sid in stop_ids:
    #     topic, msg = stop_timestamp.build_message(sid)
    #     yield topic, msg


async def main():
    producer = GenericKafkaProducer()

    try:
        old_topic = ""
        for topic, msg in generate():
            if old_topic != topic:
                print(topic)
                time.sleep(1)
            old_topic = topic
            await producer.publish(topic, msg)
    finally:
        await producer.stop()
        pass

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
