from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing
import uuid


OPERATORS_TOPIC="oras.ev.operators-modified.1"


class SennderAvroOrasEventTracing(AvroBaseModel):
    """
    Object to allow distributed tracing across services like Datadog
    """
    trace_id: str = Field(metadata={'doc': 'Identifier to allow distributed tracing across services.'})



class SennderAvroOrasEventMeta(AvroBaseModel):
    """
    Generic meta data for an event
    """
    tracing: SennderAvroOrasEventTracing
    event_id: str = Field(metadata={'doc': 'Unique identifier for each event (used to cope with duplications)'})
    created: datetime.datetime = Field(metadata={'doc': 'Time of event creation :warning: not to be mixed with when the underlying data changed in the DB.'})
    tenant: str = Field(metadata={'doc': 'SaaS Tenant / User Company'})
    source: str = Field(metadata={'doc': 'Event producer service name'})

    class Meta:
        field_order = ['event_id', 'created', 'tenant', 'source', 'tracing']



class SennderAvroOrasOperator(AvroBaseModel):
    operator_id: uuid.UUID = Field(metadata={'doc': 'usermanagement uuid of the operator assigned to a role.'})
    role: str = Field(metadata={'doc': 'Role being assigned to the operator.'})
    assigned_at: datetime.datetime = Field(metadata={'doc': 'Time the assignement occured.'})
    assigned_by: uuid.UUID = Field(metadata={'doc': 'usermanagement uuid of the person performing the assignment.'})



class SennderAvroOrasPayload(AvroBaseModel):
    modified: datetime.datetime
    operators: typing.List[SennderAvroOrasOperator]



class SennderAvroCommunicationOperatorsModifiedRecordsKey(AvroBaseModel):
    entity_name: str = Field(metadata={'doc': "Type of the entity. e.g. 'shipment'"})
    entity_id: str = Field(metadata={'doc': "ID of the entity. e.g. '$shipmentId'"})
    tenant: str = Field(metadata={'doc': 'SaaS Tenant / User Company'})



class SennderAvroOrasOperatorsModifiedRecordsValue(AvroBaseModel):
    data: SennderAvroOrasPayload
    meta: SennderAvroOrasEventMeta = Field(metadata={'doc': 'Event meta data'})

    class Meta:
        field_order = ['meta', 'data']
