from dataclasses_avroschema import types
from dataclasses_avroschema.avrodantic import AvroBaseModel
import datetime
import typing


USER_TOPIC="user-management.ev.user-changed.3"


class SennderAvroUserManagementMetadata(AvroBaseModel):
    timestamp: datetime.datetime
    type: str
    tenant: str



class SennderAvroUserManagementPayload(AvroBaseModel):
    id: str
    name: str
    email: str
    active: bool
    roles: typing.List[str]
    created_at: datetime.datetime
    updated_at: datetime.datetime
    mothership_id: typing.Optional[types.Int32] = None

    class Meta:
        field_order = ['id', 'name', 'email', 'mothership_id', 'active', 'roles', 'created_at', 'updated_at']



class SennderAvroUserManagementInternalUsersKey(AvroBaseModel):
    user_id: str
    tenant: str



class SennderAvroUserManagementInternalUsersValue(AvroBaseModel):
    meta: SennderAvroUserManagementMetadata
    data: SennderAvroUserManagementPayload
