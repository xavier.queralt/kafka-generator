from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing
import uuid


STOP_TIMESTAMP_TOPIC="single-view.ev.transit-tour-stop-timestamps.0"


class SennderAvroMatchingMetadata(AvroBaseModel):
    created: datetime.datetime
    event_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the event sent (UUID).'})
    source: str = Field(metadata={'doc': 'Source of the message'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})
    transaction_id: uuid.UUID = Field(metadata={'doc': 'Identifier to track and correlate distributed transaction calls triggered by updates with global impact.'})

    class Meta:
        field_order = ['event_id', 'created', 'source', 'tenant', 'transaction_id']



class SennderAvroMatchingTourExecutionStopTimestamps(AvroBaseModel):
    id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the transport plan stop.'})
    transport_plan_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the transport plan entity'})
    tour_execution_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the tour execution.'})
    timestamp_arrival: datetime.datetime = Field(metadata={'doc': 'The arrival timestamp when action took place, reported by the source.'})
    timestamp_departure: datetime.datetime = Field(metadata={'doc': 'The departure timestamp when action took place, reported by the source.'})
    action_completed_arrival: datetime.datetime = Field(metadata={'doc': 'The arrival timestamp when action took place, reported by the source.'})
    action_completed_departure: datetime.datetime = Field(metadata={'doc': 'The departure timestamp when action took place, reported by the source.'})
    missing_timestamp: bool = Field(metadata={'doc': 'The value after check for missing timestamps'})



class SennderAvroMatchingPayload(AvroBaseModel):
    entity: SennderAvroMatchingTourExecutionStopTimestamps
    modified: datetime.datetime = Field(metadata={'doc': 'The timestamp when the entity was modified.'})

    class Meta:
        field_order = ['modified', 'entity']



class SennderAvroMatchingSingleViewTransitTourStopTimestampKey(AvroBaseModel):
    entity_id: uuid.UUID = Field(metadata={'doc': 'UUID of the transport plan stop id'})
    entity_name: str = Field(metadata={'doc': 'Should be only transit tour stop'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})



class SennderAvroMatchingSingleViewTransitTourStopTimestampValue(AvroBaseModel):
    meta: SennderAvroMatchingMetadata
    data: SennderAvroMatchingPayload
