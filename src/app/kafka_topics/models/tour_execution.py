from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing
import uuid


TOUR_EXECUTION_TOPIC="transit.ev.tour-execution-status.1"


class SennderAvroTransitSpan(AvroBaseModel):
    """
    Represents a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.
    """
    span_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.'}, default=None)



class SennderAvroTransitTracing(AvroBaseModel):
    """
    Allows distributed tracing across services with tools like Datadog.
    """
    span: typing.Optional[SennderAvroTransitSpan]
    trace_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies an operation that happens across services. It should be passed in between services.'}, default=None)

    class Meta:
        field_order = ['trace_id', 'span']



class SennderAvroTransitMetadata(AvroBaseModel):
    tracing: typing.Optional[SennderAvroTransitTracing]
    event_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the event sent.'})
    created: datetime.datetime = Field(metadata={'doc': 'Indicates when the event was created.'})
    tenant: str = Field(metadata={'doc': "Tenant without suffix, e.g. 'sennder'."})
    source: str = Field(metadata={'doc': 'Identifier of the service publishing the message. default: transit-services.'})

    class Meta:
        field_order = ['event_id', 'created', 'tenant', 'source', 'tracing']



class SennderAvroTransitTourExecutionStatusEntity(AvroBaseModel):
    """
    Full tour execution entity.
    """
    id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the tour execution entity.'})
    shipment_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the shipment executed.'})
    tenant: str = Field(metadata={'doc': "Tenant without suffix, e.g. 'sennder'."})
    status: str = Field(metadata={'doc': 'The status of the tour execution object.'})
    transport_plan_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the planning/transport-plan entity.'})
    executed_by: typing.Optional[uuid.UUID] = Field(metadata={'doc': 'Unique identifier of user executed tour.'})
    executed_at: typing.Optional[datetime.datetime] = Field(metadata={'doc': 'Indicates when a tour execution object marked as executed.'})
    created_at: datetime.datetime = Field(metadata={'doc': 'Indicates when a tour execution object created.'})



class SennderAvroTransitTourExecutionStatusEventData(AvroBaseModel):
    entity: SennderAvroTransitTourExecutionStatusEntity
    modified: datetime.datetime = Field(metadata={'doc': 'The timestamp when the entity was modified.'})



class SennderAvroTransitTourExecutionStatusEventKey(AvroBaseModel):
    """
    Schema for key of TourExecutionStatusEvent
    """
    entity_id: uuid.UUID = Field(metadata={'doc': 'ID of the `tour_execution`.'})
    entity_name: str = Field(metadata={'doc': "Should only be 'tour_execution'."})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})



class SennderAvroTransitTourExecutionStatusEventValue(AvroBaseModel):
    """
    A schema definition for value part of a Tour Execution Status event.
    """
    meta: SennderAvroTransitMetadata
    data: SennderAvroTransitTourExecutionStatusEventData = Field(metadata={'doc': 'Data for Tour Execution Status Event.'})
