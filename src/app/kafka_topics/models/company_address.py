from dataclasses_avroschema import types
from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing


COMPANY_ADDRESS_TOPIC="ordering.ev.company-address.0"


class SennderAvroOrderingComapnyAddressKey(AvroBaseModel):
    id: types.Int32 = Field(metadata={'doc': 'Unique CompanyAddress instance identifier.'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})



class SennderAvroOrderingCompanyAdressValue(AvroBaseModel):
    modified: datetime.datetime = Field(metadata={'doc': 'Timestamp of the last update.'})
    address_id: types.Int32 = Field(metadata={'doc': 'ID of the Address object.'})
    company_id: types.Int32 = Field(metadata={'doc': 'ID of the Company object.'})
    description: typing.Optional[str] = Field(metadata={'doc': 'Short description that identifies the site or the hub.'}, default=None)
    external_id: typing.Optional[str] = Field(metadata={'doc': 'The id of the external domain model from where this instance was created. In facility domain, this will be the id of the facility profile'}, default=None)
