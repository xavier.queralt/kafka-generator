from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import enum
import typing
import uuid


FACILITY_PROFILE_TOPIC="facility.ev.profile.2"

class SennderAvroFacilityFacilityProfileEntityName(enum.Enum):
    PROFILE = "profile"



class SennderAvroFacilityProfileSpan(AvroBaseModel):
    """
    Represents a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.
    """
    span_id: str = Field(metadata={'doc': 'Identifies a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.'})



class SennderAvroFacilityProfileEventTracing(AvroBaseModel):
    """
    Object to allow distributed tracing across services like Datadog
    """
    span: SennderAvroFacilityProfileSpan
    trace_id: str = Field(metadata={'doc': 'Identifier to allow distributed tracing across services.'})

    class Meta:
        field_order = ['trace_id', 'span']



class SennderAvroFacilityProfileEventMeta(AvroBaseModel):
    """
    Generic meta data for an event
    """
    tracing: SennderAvroFacilityProfileEventTracing
    event_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier for the event'})
    created: datetime.datetime = Field(metadata={'doc': "Time of event's creation"})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant'})
    source: str = Field(metadata={'doc': 'The service identifier publishing the message'})
    transaction_id: uuid.UUID = Field(metadata={'doc': 'Identifier to track and correlate distributed transaction calls triggered by updates with global impact.'})

    class Meta:
        field_order = ['event_id', 'created', 'tenant', 'source', 'tracing', 'transaction_id']



class SennderAvroFacilityProfileAssociatedShipper(AvroBaseModel):
    name: str
    tms_link: typing.Optional[str] = None



class SennderAvroFacilityProfileContact(AvroBaseModel):
    """
    Contact info
    """
    name: str
    surname: str
    is_primary_contact: bool
    email: typing.Optional[str] = None
    phone: typing.Optional[str] = None
    role: typing.Optional[str] = None
    gender: typing.Optional[str] = None
    communication_language: typing.Optional[str] = None
    note: typing.Optional[str] = None

    class Meta:
        field_order = ['name', 'surname', 'email', 'phone', 'role', 'gender', 'communication_language', 'is_primary_contact', 'note']



class SennderAvroFacilityProfileCoordinates(AvroBaseModel):
    """
    Compatible with The GeoJSON Format, every coordinate should be compatible with the Coordinate Reference System
    defined at [RFC-7946](https://www.rfc-editor.org/rfc/rfc7946#section-4)
    """
    longitude: float
    latitude: float



class SennderAvroFacilityProfileAddress(AvroBaseModel):
    """
    Address adapted from ADR at [RFC 6350](https://www.rfc-editor.org/rfc/rfc6350.html#section-6.3.1)
    """
    street: str
    formatted: str
    postal_code: str
    country: str
    country_code: str
    coordinates: SennderAvroFacilityProfileCoordinates
    locality: typing.Optional[str] = None
    state: typing.Optional[str] = None
    state_code: typing.Optional[str] = None
    WH_code: typing.Optional[str] = None
    three_word: typing.Optional[str] = None
    note: typing.Optional[str] = None

    class Meta:
        field_order = ['street', 'formatted', 'postal_code', 'locality', 'state', 'state_code', 'WH_code', 'three_word', 'country', 'country_code', 'note', 'coordinates']



class SennderAvroFacilityProfileEntrance(AvroBaseModel):
    entry_type: str
    address: SennderAvroFacilityProfileAddress
    name: str = "TRUCKS"

    class Meta:
        field_order = ['name', 'entry_type', 'address']



class SennderAvroFacilityProfileLocation(AvroBaseModel):
    timezone: str
    address: SennderAvroFacilityProfileAddress
    entrances: typing.List[SennderAvroFacilityProfileEntrance]



class SennderAvroFacilityProfileProfile(AvroBaseModel):
    facility_types: typing.List[str]
    contacts: typing.Optional[typing.List[SennderAvroFacilityProfileContact]]
    location: SennderAvroFacilityProfileLocation
    id: uuid.UUID = Field(metadata={'doc': 'Facility UUID.'})
    name: str = Field(metadata={'doc': 'Facility name.'})
    tms_link: typing.Optional[str] = None
    associated_shippers: typing.Optional[typing.List[SennderAvroFacilityProfileAssociatedShipper]] = None
    supported_languages: typing.Optional[typing.List[str]] = Field(metadata={'doc': 'Language follow [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) two letters codes'}, default=None)

    class Meta:
        field_order = ['id', 'name', 'tms_link', 'facility_types', 'associated_shippers', 'contacts', 'location', 'supported_languages']



class SennderAvroFacilityProfileEventDataProfile(AvroBaseModel):
    """
    Facility Profile event payload
    """
    entity: SennderAvroFacilityProfileProfile = Field(metadata={'doc': 'Facility Profile Entity'})
    modified: datetime.datetime = Field(metadata={'doc': 'The timestamp when the profile entity was modified. SennSearch should use this.'})



class SennderAvroFacilityFacilityProfileKey(AvroBaseModel):
    entity_id: uuid.UUID = Field(metadata={'doc': 'Facility UUID'})
    entity_name: SennderAvroFacilityFacilityProfileEntityName = Field(metadata={'doc': 'Should be only profile.'})
    tenant: str = Field(metadata={'doc': 'Tenant where this facility belongs to'})



class SennderAvroFacilityProfileFacilityProfileValue(AvroBaseModel):
    meta: SennderAvroFacilityProfileEventMeta
    data: SennderAvroFacilityProfileEventDataProfile
