from dataclasses_avroschema import condecimal
from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import enum
import typing
import uuid


SHIPMENT_DRAFT_TOPIC="shipments.ev.shipment-draft.2"

class ShipmentEntityName(enum.Enum):
    SHIPMENT = "shipment"



class SennderAvroShipmentsSpan(AvroBaseModel):
    """
    Represents a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.
    """
    span_id: str = Field(metadata={'doc': 'Identifies a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.'})



class SennderAvroShipmentsTracing(AvroBaseModel):
    """
    Allows distributed tracing across services with tools like Datadog.
    """
    span: SennderAvroShipmentsSpan
    trace_id: str = Field(metadata={'doc': 'Identifies an operation that happens across services. It should be passed in between services.'})

    class Meta:
        field_order = ['trace_id', 'span']



class Meta(AvroBaseModel):
    tracing: SennderAvroShipmentsTracing
    event_id: uuid.UUID = Field(metadata={'doc': 'Identifies the event. Can be used to cope with duplicates.'})
    created: datetime.datetime = Field(metadata={'doc': 'Time of event creation. Not to be mixed with when the underlying data changed in the DB.'})
    tenant: str = Field(metadata={'doc': "Tenant without suffix, e.g. 'sennder'."})
    source: str = Field(metadata={'doc': "Identifies the service publishing the message. Typically 'shipments'."})

    class Meta:
        field_order = ['event_id', 'created', 'tenant', 'source', 'tracing']



class SennderAvroShipmentsMonetaryAmount(AvroBaseModel):
    """
    Monetary amount expressed in a specific currency.
    """
    number: condecimal(max_digits=20, decimal_places=2)
    currency: str = Field(metadata={'doc': 'ISO 4217 code'})



class Surcharge(AvroBaseModel):
    """
    Surcharges emitted by the shipper, not carrier or brokerage.
    """
    amount: SennderAvroShipmentsMonetaryAmount
    note: str
    type: str = Field(metadata={'doc': 'mandatory enum indicating the type of the surcharge'})

    class Meta:
        field_order = ['type', 'amount', 'note']



class Financials(AvroBaseModel):
    """
    The basic financial price and charges a shipper provides. In the future we will need to re-visit this as base_prices often come from the contract instead.
    """
    surcharges: typing.List[Surcharge]
    note: str
    base_price: typing.Optional[SennderAvroShipmentsMonetaryAmount] = None

    class Meta:
        field_order = ['base_price', 'surcharges', 'note']



class LoadingRequirement(AvroBaseModel):
    reference: str
    note: str
    loading_window_open_at: typing.Optional[datetime.datetime] = None
    loading_window_closed_at: typing.Optional[datetime.datetime] = None
    warehouse_address_id: typing.Optional[int] = Field(metadata={'doc': 'CompanyAddress ID of the warehouse'}, default=None)
    facility_profile_id: typing.Optional[uuid.UUID] = None
    warehouse_contact_id: typing.Optional[int] = None



class UnloadingRequirement(AvroBaseModel):
    reference: str
    note: str
    unloading_window_open_at: typing.Optional[datetime.datetime] = None
    unloading_window_closed_at: typing.Optional[datetime.datetime] = None
    warehouse_address_id: typing.Optional[int] = Field(metadata={'doc': 'CompanyAddress ID of the warehouse'}, default=None)
    facility_profile_id: typing.Optional[uuid.UUID] = None
    warehouse_contact_id: typing.Optional[int] = None



class TemperatureRequirement(AvroBaseModel):
    """
    The temperature requirements for the goods can determine if Frigo trucks are needed. It may be that a load does not need to be at Frigo temperatures but the contract itself requires a Frigo truck, this happens sometimes and shippers will ask us to pay a fee for contract violations. Temperatures are in degrees Celsius.
    """
    minimum_temperature: typing.Optional[condecimal(max_digits=15, decimal_places=2)] = None
    maximum_temperature: typing.Optional[condecimal(max_digits=15, decimal_places=2)] = None



class PalletExchangeRequirement(AvroBaseModel):
    """
    Pallet exchange is complicated and may grow beyond this simple boolean flag.
    """
    is_required: bool



class SealedRequirement(AvroBaseModel):
    """
    Some trucks will attach a 'seal' with a special number on it to the goods or truck loading door to identify if the load in a truck was accessed between origin and destination. Seals can be important for goods that may be prone to tampering or theft, or simply by shipper preference.
    """
    is_required: bool



class WarehouseLoadingRequirement(AvroBaseModel):
    """
    The type of loading is important. Sometimes we send coca-cola a 33 pallet capacity semi truck for 2 pallets (and the truck runs with 31 empty spots), because the warehouse is not set up to load a bunch of coke bottles onto the roof of my Ford Fiesta at the loading dock, or if we move a ton of loose sand they will need to load it from the top, not the side, it would just spill out on the ground, and the folks at the coca-cola warehouse don't like me building sandcastles outside of their loading docks.
    """
    top_loading: bool
    side_loading: bool
    dock_loading: bool



class VehicleRequirement(AvroBaseModel):
    """
    This is limited to the requirements a shipper themselves emits. They may implicitly request a tour where the only way to deliver it on time needs 2 drivers and a ferry, but that conclusion is made by the brokerage (sennder) or a carrier and not the shipper themselves.
    """
    preferred_vehicle_type: str
    acceptable_replacement_vehicle_types: typing.List[str]
    code_xl: bool
    cleaned: bool



class CertificationRequirement(AvroBaseModel):
    """
    This is about requirements imposed by the shipper that affect not only the vehicle, the driver or the loading/unloading alone, but multiple parts of the transport process. For example, safe food & drugs movement imposes requirements on the carrier company, on their vehicles, on their drivers, and possibly on the warehouse.
    """
    food_certified: bool
    waste_certified: bool
    dangerous_goods_certified: bool



class DriverRequirement(AvroBaseModel):
    """
    The requirements a shipper exposes on a driver.
    """
    has_safety_vest: bool



class TrackingRequirement(AvroBaseModel):
    """
    Shippers have ERP systems we send GPS telemetry to while a tour is on-going, and they need a special identifier that they give us so we can match GPS from an arbitrary truck to their load.
    """
    tracking_id_for_shipper: str
    is_required: bool



class LoadRequirements(AvroBaseModel):
    """
    Requirements specific for a given Load.
    """
    loading_requirements: typing.List[LoadingRequirement]
    unloading_requirements: typing.List[UnloadingRequirement]
    temperature_requirement: typing.Optional[TemperatureRequirement] = None
    pallet_exchange_requirement: typing.Optional[PalletExchangeRequirement] = None
    sealed_requirement: typing.Optional[SealedRequirement] = None
    warehouse_loading_requirement: typing.Optional[WarehouseLoadingRequirement] = None
    vehicle_requirement: typing.Optional[VehicleRequirement] = None
    certification_requirement: typing.Optional[CertificationRequirement] = None
    driver_requirement: typing.Optional[DriverRequirement] = None
    tracking_requirement: typing.Optional[TrackingRequirement] = None



class LoadUnit(AvroBaseModel):
    """
    It is common to have many items in a load of the same kind, for example if there is a pallet full of pancakes you would describe one pallet here, and in the parent Load entity specifies the quantity of that item in the load. In case of bulk cargo, the load unit can be for example 1 ton of sand.
    """
    type: str
    description: str
    height_in_meters: typing.Optional[float] = None
    length_in_meters: typing.Optional[float] = None
    width_in_meters: typing.Optional[float] = None
    weight_in_kg: typing.Optional[float] = None



class Load(AvroBaseModel):
    """
    A load is a specific set of goods that will be moved from one place to another. Loads are concrete specific goods which a shipper attaches requirements on to, including not only when they need to be picked up and dropped off, but also things like what kind of truck is needed to handle those loads, etc. A Shipment is composed of many loads. A truck may handle lots of loads at once. There are also situations where a load may be handled by multiple carriers. Total weight gets precedence over load unit weight.
    """
    load_requirements: LoadRequirements
    quantity: typing.Optional[condecimal(max_digits=15, decimal_places=6)] = None
    load_unit: typing.Optional[LoadUnit] = None
    total_weight_in_kg: typing.Optional[float] = None



class Contract(AvroBaseModel):
    type: str = Field(metadata={'enum': ['REGULAR', 'RECURRING', 'SPOT', 'SCALED_SPOT'], 'doc': 'Shipper contract type'})



class Shipper(AvroBaseModel):
    """
    Information about the shipper company and contact.
    """
    contact_id: int
    shipper_id: int = Field(metadata={'doc': 'Company ID of the shipper'})
    contract: Contract = Field(metadata={'doc': 'Shipper contract information'})

    class Meta:
        field_order = ['shipper_id', 'contact_id', 'contract']



class ShipmentRequirements(AvroBaseModel):
    direct_delivery: bool = Field(metadata={'doc': 'Indicates that the shipment needs to be delivered without the driver taking extra rest time.'})
    arrival_notification: bool = Field(metadata={'doc': 'Indicates that the shipper needs to be notified when the truck reaches the last delivery stop.'})



class ShipmentMeta(AvroBaseModel):
    """
    Relationships to IDs from other systems.
    """
    creator: str
    order_id: typing.Optional[int] = None
    contract_id: int
    transporeon_id: typing.Optional[int] = None
    client_app: str = Field(metadata={'doc': 'Client application that created the Draft (e.g: mastersheet)'})

    class Meta:
        field_order = ['order_id', 'contract_id', 'transporeon_id', 'creator', 'client_app']



class Shipment(AvroBaseModel):
    """
    Set of requirements from a shipper. Root object of the shipment creation API. A shipment is a container of loads and other *shipper-provided* details, like financials.
    """
    financials: Financials
    loads: typing.List[Load]
    shipper: Shipper
    shipment_meta: ShipmentMeta
    reference: str
    shipment_requirements: typing.Optional[ShipmentRequirements] = None

    class Meta:
        field_order = ['financials', 'loads', 'shipper', 'shipment_requirements', 'shipment_meta', 'reference']



class Data(AvroBaseModel):
    """
    Domain-specific contents of the message.
    """
    entity: Shipment
    action: str
    created: datetime.datetime
    modified: datetime.datetime



class ShipmentKey(AvroBaseModel):
    entity_name: ShipmentEntityName = ShipmentEntityName.SHIPMENT
    entity_id: uuid.UUID = Field(metadata={'doc': 'Unique shipment identifier (UUID).'})
    tenant: str = Field(metadata={'doc': "Tenant without suffix, e.g. 'sennder'."})

    class Meta:
        field_order = ['entity_id', 'entity_name', 'tenant']



class ShipmentDraftValue(AvroBaseModel):
    """
    We don't differentiate between missing value, null, empty array and empty string within this schema. All of them mean 'not specified'.
    """
    meta: Meta
    data: Data
