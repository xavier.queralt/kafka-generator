from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing
import uuid


CARRIER_PROFILE_TOPIC="carrier-profile.ev.carrier.1"


class SennderAvroCarrierProfileEventTracing(AvroBaseModel):
    """
    Object to allow distributed tracing across services like Datadog
    """
    trace_id: uuid.UUID = Field(metadata={'doc': 'Identifier to allow distributed tracing across services.'})



class SennderAvroCarrierProfileEventMeta(AvroBaseModel):
    """
    Generic meta data for an event
    """
    tracing: SennderAvroCarrierProfileEventTracing
    event_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier for each event (used to cope with duplications)'})
    created: datetime.datetime = Field(metadata={'doc': 'Time of event creation :warning: not to be mixed with when the underlyingdata changed in the DB.'})
    tenant: str = Field(metadata={'doc': 'SaaS Tenant / User Company'})
    source: str = Field(metadata={'doc': 'Event producer service name'})
    origin: str = Field(metadata={'doc': 'System owning the carrier at the beginning of a request'})

    class Meta:
        field_order = ['event_id', 'created', 'tenant', 'source', 'tracing', 'origin']



class SennderAvroCarrierProfileRawAddress(AvroBaseModel):
    city: typing.Optional[str] = Field(metadata={'doc': 'City name'}, default=None)
    country: typing.Optional[str] = Field(metadata={'doc': 'ISO 3166-1 alpha-2 country code'}, default=None)
    postal_code: typing.Optional[str] = Field(metadata={'doc': 'Postal Code'}, default=None)
    state_code: typing.Optional[str] = Field(metadata={'doc': 'ISO_3166-2 country subdivision code'}, default=None)
    street: typing.Optional[str] = Field(metadata={'doc': 'Street and unit number'}, default=None)



class SennderAvroCarrierProfileVAT(AvroBaseModel):
    """
    VAT (Value-Added Tax) Information
    """
    country_code: typing.Optional[str] = Field(metadata={'doc': 'VAT country code (as used by VIES service)'}, default=None)
    number: typing.Optional[str] = Field(metadata={'doc': 'Value-added tax identification number (including country code prefix)\n(see https://en.wikipedia.org/wiki/VAT_identification_number for details)'}, default=None)
    vies_request_status: typing.Optional[bool] = Field(metadata={'doc': 'Result of VIES request'}, default=None)
    vies_requested_at: typing.Optional[datetime.datetime] = Field(metadata={'doc': 'Date and time of VIES request'}, default=None)



class SennderAvroCarrierProfileBillingInfo(AvroBaseModel):
    address: SennderAvroCarrierProfileRawAddress = Field(metadata={'doc': 'Billing address'})
    email: typing.Optional[str] = Field(metadata={'doc': 'Billing email address'}, default=None)
    instructions: typing.Optional[str] = Field(metadata={'doc': 'Additional billing instructions'}, default=None)
    accounting_id: typing.Optional[int] = Field(metadata={'doc': 'Unique ID used by NetSuite to manage payments for the carrier'}, default=None)
    invoicing_term_days: typing.Optional[int] = Field(metadata={'doc': 'Invoicing term in days'}, default=None)
    discount_percent: typing.Optional[float] = Field(metadata={'doc': 'Discount (aka Skonto) to be applied in percentage'}, default=None)
    iban: typing.Optional[str] = Field(metadata={'doc': 'International Bank Account Number (IBAN) for direct payments'}, default=None)
    bic: typing.Optional[str] = Field(metadata={'doc': 'SWIFT Business Identifier Code (BIC) for direct payments'}, default=None)
    factoring_company: typing.Optional[str] = Field(metadata={'doc': 'Factoring provider company name'}, default=None)
    factoring_iban: typing.Optional[str] = Field(metadata={'doc': 'International Bank Account Number (IBAN) for payments via a factoring provider'}, default=None)
    factoring_bic: typing.Optional[str] = Field(metadata={'doc': 'SWIFT Business Identifier Code (BIC) for payments via a factoring provider'}, default=None)
    mandato_fatturazione_in_nome_e_per_conto: bool = Field(metadata={'doc': 'Mandato fatturazione in nome e per conto (Poste Italiane only)'}, default=False)



class SennderAvroCarrierProfileFleetOverview(AvroBaseModel):
    """
    Fleet Overview provides high-level information as to the size of a Carrier's fleet
    and the vehicles within it, but is purely informative and should not be used for
    programmatic purposes requiring an exact number.
    """
    num_vans: typing.Optional[int] = Field(metadata={'doc': 'Number of vans'}, default=None)
    num_3_5t_trucks: typing.Optional[int] = Field(metadata={'doc': 'Number of 3.5 tonne trucks'}, default=None)
    num_7_5t_trucks: typing.Optional[int] = Field(metadata={'doc': 'Number of 7.5 tonne trucks'}, default=None)
    num_12t_trucks: typing.Optional[int] = Field(metadata={'doc': 'Number of 12 tonne trucks'}, default=None)
    num_40t_mega_trucks: typing.Optional[int] = Field(metadata={'doc': 'Number of 40 tonne Mega trucks'}, default=None)
    num_40t_tautliner_trucks: typing.Optional[int] = Field(metadata={'doc': 'Number of 40 tonne Tautliner trucks'}, default=None)
    num_40t_open_trucks: typing.Optional[int] = Field(metadata={'doc': 'Number of 40 tonne Open trucks'}, default=None)
    num_40t_frigo_trucks: typing.Optional[int] = Field(metadata={'doc': 'Number of 40 tonne Frigo trucks'}, default=None)
    num_container_trucks: typing.Optional[int] = Field(metadata={'doc': 'Number of container trucks'}, default=None)
    num_two_swap_body_trucks: typing.Optional[int] = Field(metadata={'doc': 'Number of two-swap body trucks'}, default=None)
    num_box_trailers: typing.Optional[int] = Field(metadata={'doc': 'Number of box trailers'}, default=None)



class SennderAvroCarrierProfileGPSIntegration(AvroBaseModel):
    """
    GPS integration currently works as follows:
    
    Option 1: Individual trucks and trailers are integrated with a physical telemetry system
    (there are thousands of them) and then an invite is sent to the carrier to
    perform self sign-on with an aggregator (eg. CO3 or Project 44). Relevant
    information for this onboarding process is then recorded here to keep track
    of which carriers are integrated with which services.
    
    Option 2: The carriers have one (or more) of their drivers install the sennder
    Driver App and use it for a load. A successful integration is also recorded here.
    
    It should be noted that while we record that a carrier is capable of performing
    GPS tracked loads there is no guarantee that all trucks or trailers associated
    with that carrier are actually integrated.
    """
    co3_integration_funnel_entry_at: typing.Optional[datetime.datetime] = Field(metadata={'doc': 'Date and time when the carrier was added to CO3 integration funnel'}, default=None)
    co3_integration_status: typing.Optional[str] = Field(metadata={'doc': 'CO3 Integration status'}, default=None)
    co3_integration_completed_at: typing.Optional[datetime.datetime] = Field(metadata={'doc': 'Date and time when the carrier completed integrated with CO3'}, default=None)
    co3_integration_notes: typing.Optional[str] = Field(metadata={'doc': 'Additional notes related to CO3 integration process'}, default=None)
    project_44_integration_completed_at: typing.Optional[datetime.datetime] = Field(metadata={'doc': 'Date and time when the carrier completed integration with Project 44'}, default=None)
    project_44_carrier_id: typing.Optional[str] = Field(metadata={'doc': 'DEPRECATED: Use project_44_carrier_ids instead.'}, default=None)
    project_44_carrier_ids: typing.Optional[typing.List[str]] = Field(metadata={'doc': 'Project 44 carrier IDs'}, default=None)
    sennder_app_integration_completed_at: typing.Optional[datetime.datetime] = Field(metadata={'doc': 'Date and time when the carrier completed integration with the sennder App'}, default=None)



class SennderAvroCarrierProfileCertifications(AvroBaseModel):
    """
    Certifications that a carrier has
    """
    adr: typing.Optional[str] = Field(metadata={'doc': 'ADR certification status'}, default=None)
    haccp: typing.Optional[str] = Field(metadata={'doc': 'HACCP (Food and Beverage) certification status'}, default=None)
    tapa: typing.Optional[str] = Field(metadata={'doc': 'TAPA certification status'}, default=None)



class SennderAvroCarrierProfileCarrier(AvroBaseModel):
    """
    Defines core information for a Carrier
    
    A carrier is defined as a company which is capable of fulfilling the delivery of
    a load. Carriers have a wide range of data associated with them such as Contacts, Documents,
    Truck Yards, etc.
    """
    vat: SennderAvroCarrierProfileVAT
    billing: SennderAvroCarrierProfileBillingInfo
    fleet_overview: SennderAvroCarrierProfileFleetOverview
    gps_integration: SennderAvroCarrierProfileGPSIntegration
    id: uuid.UUID = Field(metadata={'doc': 'Unique identifier'})
    tenant: str = Field(metadata={'doc': 'SaaS Tenant / User Company'})
    created_at: datetime.datetime = Field(metadata={'doc': 'Date when object was first created'})
    updated_at: datetime.datetime = Field(metadata={'doc': 'Date when object was last updated'})
    deleted_at: typing.Optional[datetime.datetime] = Field(metadata={'doc': 'Date when object was marked as deleted'}, default=None)
    last_modified_by: uuid.UUID = Field(metadata={'doc': 'ID of the latest user to modify the object'})
    type: str = Field(metadata={'doc': 'Type of the carrier\n\n`CARRIER` - A company which owns/operates trucks/trailers and is able to \ndeliver loads.\n\n`CHARTERING_OFFICE` - A sennder internal carrier used for chartering.\n\n`FREIGHT_FORWARDER` - A company which sub-contracts loads to other carriers \n(license not required).\n\n`OTHER` - Some other type of company.\n'})
    source: str = Field(metadata={'doc': 'Source of carrier data'})
    sub_source: typing.Optional[str] = Field(metadata={'doc': 'Sub-category of carrier source'}, default=None)
    cooperation_type: typing.Optional[typing.List[str]] = Field(metadata={'doc': 'How the carrier would like to take on loads (from a business perspective)'}, default=None)
    company_name: str = Field(metadata={'doc': 'Business name of the carrier'})
    website: typing.Optional[str] = Field(metadata={'doc': 'Company website'}, default=None)
    phone: typing.Optional[str] = Field(metadata={'doc': 'Business phone number'}, default=None)
    email: typing.Optional[str] = Field(metadata={'doc': 'Company email address'}, default=None)
    certified_email: typing.Optional[str] = Field(metadata={'doc': 'Certified email address'}, default=None)
    traffic_office_email: typing.Optional[str] = Field(metadata={'doc': 'Traffic office email address'}, default=None)
    address: SennderAvroCarrierProfileRawAddress = Field(metadata={'doc': 'Business address'})
    legal_representative: typing.Optional[str] = Field(metadata={'doc': 'Name of the legal representative of the carrier \n(currently used for sennpay and Poste Italiane)'}, default=None)
    emergency_phone: typing.Optional[str] = Field(metadata={'doc': 'Emergency phone number'}, default=None)
    emergency_notes: typing.Optional[str] = Field(metadata={'doc': 'Extra notes in the case of an emergency'}, default=None)
    tags: typing.Optional[typing.List[str]] = Field(metadata={'doc': 'Additional tags'}, default=None)
    carrier_manager: typing.Optional[uuid.UUID] = Field(metadata={'doc': 'User ID of the carrier manager'}, default=None)
    referrer: typing.Optional[uuid.UUID] = Field(metadata={'doc': 'User ID of the referrer'}, default=None)
    notes: typing.Optional[str] = Field(metadata={'doc': 'Additional notes'}, default=None)
    salesforce_id: typing.Optional[str] = Field(metadata={'doc': 'Salesforce Account ID\nNOTE: To be deprecated. Only use for migration from Mothership'}, default=None)
    mothership_id: typing.Optional[int] = Field(metadata={'doc': 'Mothership Carrier ID\nNOTE: To be deprecated. Only use for migration from Mothership'}, default=None)
    timocom_id: typing.Optional[int] = Field(metadata={'doc': 'Timocom ID (used in Freight Exchange situations)'}, default=None)
    is_denylisted: bool = Field(metadata={'doc': 'Indicates if the carrier should be denied access to sennder services'}, default=False)
    reason_for_denylisting: typing.Optional[str] = Field(metadata={'doc': 'Reason for denylisting'}, default=None)
    vetting_status: str = Field(metadata={'doc': 'Onboarding vetting status'}, default="UNVETTED")
    data_owner: str = Field(metadata={'doc': 'Technical field indicating the system having write rights on a carrier.\nNOTE: Should not be set by internal services.'}, default="CPS")
    certifications: typing.Optional[SennderAvroCarrierProfileCertifications] = None

    class Meta:
        field_order = ['id', 'tenant', 'created_at', 'updated_at', 'deleted_at', 'last_modified_by', 'type', 'source', 'sub_source', 'cooperation_type', 'company_name', 'website', 'phone', 'email', 'certified_email', 'traffic_office_email', 'address', 'legal_representative', 'emergency_phone', 'emergency_notes', 'tags', 'carrier_manager', 'referrer', 'notes', 'salesforce_id', 'mothership_id', 'timocom_id', 'is_denylisted', 'reason_for_denylisting', 'vetting_status', 'data_owner', 'vat', 'billing', 'fleet_overview', 'gps_integration', 'certifications']



class SennderAvroCarrierProfileEventDataCarrier(AvroBaseModel):
    entity: SennderAvroCarrierProfileCarrier
    modified: datetime.datetime
    action: str = Field(metadata={'doc': 'Type of action (eg. create/delete)'})



class SennderAvroCarrierProfileEventKeyCarrier(AvroBaseModel):
    entity_name: str
    entity_id: uuid.UUID = Field(metadata={'doc': 'Entity id'})
    tenant: str = Field(metadata={'doc': 'SaaS Tenant / User Company'})



class SennderAvroCarrierProfileEventValueCarrier(AvroBaseModel):
    meta: SennderAvroCarrierProfileEventMeta = Field(metadata={'doc': 'Event meta data'})
    data: SennderAvroCarrierProfileEventDataCarrier = Field(metadata={'doc': 'Event entity payload'})
