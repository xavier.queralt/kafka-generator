from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing


CARRIER_ASSIGNMENT_TOPIC="carrier_assign.ev.carrier-assignment.4"


class SennderAvroCarrierAssignMetadata(AvroBaseModel):
    modified: datetime.datetime
    type: str = Field(metadata={'doc': 'Event type (created | updated)'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})



class SennderAvroCarrierAssignCarrierRecord(AvroBaseModel):
    id: str = Field(metadata={'doc': 'Carrier unique identifier.'})
    contact_id: str = Field(metadata={'doc': 'Carrier contact unique identifier.'})



class SennderAvroCarrierAssignShipmentRecord(AvroBaseModel):
    id: str = Field(metadata={'doc': 'Shipment unique identifier.'})



class SennderAvroCarrierAssignTransportofferRecord(AvroBaseModel):
    id: str = Field(metadata={'doc': 'Transport offer unique identifier.'})



class SennderAvroCarrierAssignMonetaryAmount(AvroBaseModel):
    """
    Monetary amount expressed in a specific currency.
    """
    number: str
    currency: str = Field(metadata={'doc': 'ISO 4217 code'})



class SennderAvroCarrierAssignPayload(AvroBaseModel):
    carrier: SennderAvroCarrierAssignCarrierRecord
    shipment: SennderAvroCarrierAssignShipmentRecord
    transportoffer: SennderAvroCarrierAssignTransportofferRecord
    cost: SennderAvroCarrierAssignMonetaryAmount
    created: datetime.datetime = Field(metadata={'doc': "Timestamp of the record's creating time."})
    assignment_type: str = Field(metadata={'doc': 'Assignment type (MANUAL | AUTO)'})
    state: str = Field(metadata={'doc': 'Current assignment state (PENDING | ACCEPTED | REJECTED | EXPIRED | CANCELLED | AWAITING_MATCHING_CONFIRMATION)'})
    update_reason: typing.Optional[str] = Field(metadata={'doc': "Reason why the Assignment got updated: ('ACCEPTED', 'MATCHING_REJECTED_CAN_BE_RETRIED', 'MATCHING_REJECTED_CANT_BE_RETRIED')"}, default=None)
    valid_until: typing.Optional[datetime.datetime] = Field(metadata={'doc': 'Timestamp when the assignment is not valid anymore and the order can be assigned to another carrier.'}, default=None)
    operator_id: str = Field(metadata={'doc': 'Operator who created the assignment'})
    order_id: typing.Optional[int] = Field(metadata={'doc': 'Mothership order id.'}, default=None)
    pickup_start: datetime.datetime = Field(metadata={'doc': 'Timestamp when the assignment is first loading window is opening.'})

    class Meta:
        field_order = ['created', 'assignment_type', 'state', 'update_reason', 'valid_until', 'operator_id', 'carrier', 'shipment', 'transportoffer', 'cost', 'order_id', 'pickup_start']



class SennderAvroCarrierAssignCarrierAssignCarrierAssignmentKey(AvroBaseModel):
    shipmentcarrierassignment_id: str = Field(metadata={'doc': 'Unique assignment identifier.'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})



class SennderAvroCarrierAssignCarrierAssignCarrierAssignmentValue(AvroBaseModel):
    meta: SennderAvroCarrierAssignMetadata
    data: SennderAvroCarrierAssignPayload
