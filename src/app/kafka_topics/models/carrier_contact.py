from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing
import uuid


CARRIER_CONTACT_TOPIC="carrier-profile.ev.contact.1"


class SennderAvroCarrierProfileEventTracing(AvroBaseModel):
    """
    Object to allow distributed tracing across services like Datadog
    """
    trace_id: uuid.UUID = Field(metadata={'doc': 'Identifier to allow distributed tracing across services.'})



class SennderAvroCarrierProfileEventMeta(AvroBaseModel):
    """
    Generic meta data for an event
    """
    tracing: SennderAvroCarrierProfileEventTracing
    event_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier for each event (used to cope with duplications)'})
    created: datetime.datetime = Field(metadata={'doc': 'Time of event creation :warning: not to be mixed with when the underlyingdata changed in the DB.'})
    tenant: str = Field(metadata={'doc': 'SaaS Tenant / User Company'})
    source: str = Field(metadata={'doc': 'Event producer service name'})
    origin: str = Field(metadata={'doc': 'System owning the carrier at the beginning of a request'})

    class Meta:
        field_order = ['event_id', 'created', 'tenant', 'source', 'tracing', 'origin']



class SennderAvroCarrierProfileRawAddress(AvroBaseModel):
    city: typing.Optional[str] = Field(metadata={'doc': 'City name'}, default=None)
    country: typing.Optional[str] = Field(metadata={'doc': 'ISO 3166-1 alpha-2 country code'}, default=None)
    postal_code: typing.Optional[str] = Field(metadata={'doc': 'Postal Code'}, default=None)
    state_code: typing.Optional[str] = Field(metadata={'doc': 'ISO_3166-2 country subdivision code'}, default=None)
    street: typing.Optional[str] = Field(metadata={'doc': 'Street and unit number'}, default=None)



class SennderAvroCarrierProfileContact(AvroBaseModel):
    """
    TODO: Description
    """
    id: uuid.UUID = Field(metadata={'doc': 'Unique identifier'})
    tenant: str = Field(metadata={'doc': 'SaaS Tenant / User Company'})
    created_at: datetime.datetime = Field(metadata={'doc': 'Date when object was first created'})
    updated_at: datetime.datetime = Field(metadata={'doc': 'Date when object was last updated'})
    deleted_at: typing.Optional[datetime.datetime] = Field(metadata={'doc': 'Date when object was marked as deleted'}, default=None)
    last_modified_by: uuid.UUID = Field(metadata={'doc': 'ID of the latest user to modify the object'})
    salutation: typing.Optional[str] = Field(metadata={'doc': 'Salutation'}, default=None)
    first_name: typing.Optional[str] = Field(metadata={'doc': 'First name'}, default=None)
    last_name: typing.Optional[str] = Field(metadata={'doc': 'Last name'}, default=None)
    email: typing.Optional[str] = Field(metadata={'doc': 'Email address'}, default=None)
    phone: typing.Optional[str] = Field(metadata={'doc': 'Phone number (E164 format)'}, default=None)
    mobile_phone: typing.Optional[str] = Field(metadata={'doc': 'Mobile phone number (E164 format)'}, default=None)
    mailing_address: SennderAvroCarrierProfileRawAddress = Field(metadata={'doc': 'Mailing address'})
    language: typing.Optional[str] = Field(metadata={'doc': 'Communication language in ISO 639-1 format'}, default=None)
    role: typing.Optional[str] = Field(metadata={'doc': 'Role of the contact'}, default=None)
    is_primary: bool = Field(metadata={'doc': 'Indicates if the contact is a primary contact for the carrier'}, default=False)
    notes: typing.Optional[str] = Field(metadata={'doc': 'Additional notes'}, default=None)
    tag_status: str = Field(metadata={'doc': 'Tagging used for management purposes'}, default="UNTAGGED")
    owner: typing.Optional[uuid.UUID] = Field(metadata={'doc': 'Associated contact owner'}, default=None)
    salesforce_id: typing.Optional[str] = Field(metadata={'doc': 'Salesforce Contact ID\nNOTE: To be deprecated. Only use for migration from Mothership'}, default=None)
    mothership_id: typing.Optional[int] = Field(metadata={'doc': 'Mothership Contact ID\nNOTE: To be deprecated. Only use for migration from Mothership'}, default=None)
    carrier_id: uuid.UUID = Field(metadata={'doc': 'Associated carrier ID'})



class SennderAvroCarrierProfileEventDataContact(AvroBaseModel):
    entity: SennderAvroCarrierProfileContact
    modified: datetime.datetime
    action: str = Field(metadata={'doc': 'Type of action (eg. create/delete)'})



class SennderAvroCarrierProfileEventKeyContact(AvroBaseModel):
    entity_name: str
    entity_id: uuid.UUID = Field(metadata={'doc': 'Entity id'})
    tenant: str = Field(metadata={'doc': 'SaaS Tenant / User Company'})



class SennderAvroCarrierProfileEventValueContact(AvroBaseModel):
    meta: SennderAvroCarrierProfileEventMeta = Field(metadata={'doc': 'Event meta data'})
    data: SennderAvroCarrierProfileEventDataContact = Field(metadata={'doc': 'Event entity payload'})
