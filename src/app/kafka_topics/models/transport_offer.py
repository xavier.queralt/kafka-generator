from dataclasses_avroschema import types
from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import enum
import typing
import uuid


TRANSPORT_OFFER_TOPIC="matching.ev.transport-offer.2"


class SennderAvroMatchingTransportofferSpan(AvroBaseModel):
    """
    Represents a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.
    """
    span_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.'}, default=None)



class SennderAvroMatchingTransportofferTracing(AvroBaseModel):
    """
    Allows distributed tracing across services with tools like Datadog.
    """
    span: typing.Optional[SennderAvroMatchingTransportofferSpan]
    trace_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies an operation that happens across services. It should be passed in between services.'}, default=None)

    class Meta:
        field_order = ['trace_id', 'span']



class SennderAvroMatchingMetadata(AvroBaseModel):
    created: datetime.datetime
    tracing: typing.Optional[SennderAvroMatchingTransportofferTracing]
    event_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the event sent (UUID).'})
    type: str = Field(metadata={'doc': 'Event type (created | updated)'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})
    source: str = Field(metadata={'doc': "The Transport Offer source is always 'matching'"})
    transaction_id: uuid.UUID = Field(metadata={'doc': 'Identifier to track and correlate distributed transaction calls triggered by updates with global impact.'})
    version: types.Int32 = Field(metadata={'doc': 'Transport Offer version, starting from 1'})

    class Meta:
        field_order = ['event_id', 'created', 'type', 'tenant', 'source', 'transaction_id', 'version', 'tracing']



class SennderAvroMatchingTransportOfferRoute(AvroBaseModel):
    """
    Transport offer route (for internal usage).
    """
    route_id: str
    route_hash: str = Field(metadata={'doc': "hash taken from all the route's stop points"})
    facility_ids: typing.List[str] = Field(default_factory=list,metadata={'doc': 'Route stop point (facilities) IDs'})



class SennderAvroMatchingTransportofferMonetaryAmount(AvroBaseModel):
    """
    Monetary amount expressed in a specific currency.
    """
    number: str
    currency: str = Field(metadata={'doc': 'ISO 4217 code'})



class SennderAvroMatchingSurcharge(AvroBaseModel):
    """
    Surcharges emitted by the shipper, not carrier or brokerage.
    """
    amount: SennderAvroMatchingTransportofferMonetaryAmount
    note: str
    type: str = Field(metadata={'doc': 'mandatory enum indicating the type of the surcharge'})

    class Meta:
        field_order = ['type', 'amount', 'note']



class SennderAvroMatchingFinancials(AvroBaseModel):
    """
    The basic financial price and charges a shipper provides. In the future we will need to re-visit this as base_prices often come from the contract instead.
    """
    cost: SennderAvroMatchingTransportofferMonetaryAmount
    surcharges: typing.List[SennderAvroMatchingSurcharge]
    note: str



class SennderAvroMatchingTransportofferCustomRequirementsItem(AvroBaseModel):
    requirement_name: str = Field(metadata={'doc': "Requirements' name"})
    is_required: bool = True



class SennderAvroMatchingTransportofferLoadingRequirements(AvroBaseModel):
    """
    Loading requirements to the driver and the vehicle.
    """
    custom_requirements: typing.List[SennderAvroMatchingTransportofferCustomRequirementsItem]
    vehicle_type: str = Field(metadata={'doc': 'Vehicle Type (BUS | TRUCK_40_TAUTLINER | ...)'})
    vehicle_code_xl: bool = False
    vehicle_cleaned: bool = False
    vehicle_top_load: bool = False
    vehicle_side_load: bool = False
    vehicle_dock_load: bool = False
    vehicle_sealable: bool = False
    vehicle_pallet_exchange: bool = False
    vehicle_is_temperature_controlled: bool = False
    vehicle_min_temperature: typing.Optional[types.Float32] = None
    vehicle_max_temperature: typing.Optional[types.Float32] = None
    vehicle_food_certified: bool = False
    vehicle_waste_certified: bool = False
    vehicle_dangerous_goods_certified: bool = False
    vehicle_trackable: bool = False
    acceptable_replacement_vehicle_types: typing.List[str] = Field(default_factory=list,metadata={'doc': "Example: ['BUS', 'TRUCK_40_TAUTLINER', ... ]"})
    driver_has_safety_vest: bool = False

    class Meta:
        field_order = ['vehicle_type', 'vehicle_code_xl', 'vehicle_cleaned', 'vehicle_top_load', 'vehicle_side_load', 'vehicle_dock_load', 'vehicle_sealable', 'vehicle_pallet_exchange', 'vehicle_is_temperature_controlled', 'vehicle_min_temperature', 'vehicle_max_temperature', 'vehicle_food_certified', 'vehicle_waste_certified', 'vehicle_dangerous_goods_certified', 'vehicle_trackable', 'acceptable_replacement_vehicle_types', 'driver_has_safety_vest', 'custom_requirements']


class SennderAvroMatchingTransportofferSlotType(enum.Enum):
    FIXED_DATE_TIME = "FIXED_DATE_TIME"
    FIXED_DATE = "FIXED_DATE"
    DATE_WINDOW = "DATE_WINDOW"
    DATE_TIME_WINDOW = "DATE_TIME_WINDOW"



class SennderAvroMatchingTransportofferTimeSlot(AvroBaseModel):
    """
    Timeslot, aggregated from all the loads' timeslots
    """
    slot_type: SennderAvroMatchingTransportofferSlotType
    start: datetime.datetime
    end: typing.Optional[datetime.datetime] = None



class SennderAvroMatchingFacility(AvroBaseModel):
    """
    Facility reference, load reference, and communicated timeslot
    """
    facility_id: str
    time_slot: SennderAvroMatchingTransportofferTimeSlot
    contact_id: typing.Optional[uuid.UUID] = None
    timezone: str = Field(metadata={'doc': 'Timezone, e.g. CET or Portugal'})

    class Meta:
        field_order = ['facility_id', 'contact_id', 'timezone', 'time_slot']



class SennderAvroMatchingTransportOfferLoadDetail(AvroBaseModel):
    description: typing.Optional[str] = None
    height_in_meters: typing.Optional[types.Float32] = None
    length_in_meters: typing.Optional[types.Float32] = None
    width_in_meters: typing.Optional[types.Float32] = None
    weight_in_kg: typing.Optional[types.Float32] = None
    type: typing.Optional[str] = None


class SennderAvroMatchingTransportofferActionType(enum.Enum):
    LOADING = "LOADING"
    UNLOADING = "UNLOADING"


class SennderAvroMatchingTransportofferLoadingMethod(enum.Enum):
    TOP_LOADING = "TOP_LOADING"
    SIDE_LOADING = "SIDE_LOADING"
    DOCK_LOADING = "DOCK_LOADING"


class SennderAvroMatchingTransportofferSchedulingType(enum.Enum):
    PRE_SCHEDULED = "PRE_SCHEDULED"
    OVERNIGHT_LOAD = "OVERNIGHT_LOAD"
    FIXED_DATE_FIXED_SLOT = "FIXED_DATE_FIXED_SLOT"
    FIXED_DATE_FLEXIBLE_SLOT = "FIXED_DATE_FLEXIBLE_SLOT"
    FLEXIBLE_DATE_FIXED_SLOT = "FLEXIBLE_DATE_FIXED_SLOT"
    FLEXIBLE_DATE_FLEXIBLE_SLOT = "FLEXIBLE_DATE_FLEXIBLE_SLOT"
    FIXED_TIME = "FIXED_TIME"
    FLEXIBLE_TIME = "FLEXIBLE_TIME"


class SennderAvroFacilityPlanningSlotType(enum.Enum):
    FIXED_DATE_TIME = "FIXED_DATE_TIME"
    FIXED_DATE = "FIXED_DATE"
    DATE_WINDOW = "DATE_WINDOW"
    DATE_TIME_WINDOW = "DATE_TIME_WINDOW"



class SennderAvroFacilityPlanningTimeSlot(AvroBaseModel):
    slot_type: SennderAvroFacilityPlanningSlotType
    start: datetime.datetime
    end: typing.Optional[datetime.datetime] = None



class SennderAvroMatchingTransportofferAction(AvroBaseModel):
    facility_id: uuid.UUID
    action_shipper_reference_code: str
    action_type: SennderAvroMatchingTransportofferActionType
    loading_methods: typing.List[SennderAvroMatchingTransportofferLoadingMethod]
    scheduling_type: SennderAvroMatchingTransportofferSchedulingType = SennderAvroMatchingTransportofferSchedulingType.PRE_SCHEDULED
    note: str
    communicated_timeslot: typing.Optional[SennderAvroFacilityPlanningTimeSlot] = None

    class Meta:
        field_order = ['facility_id', 'action_shipper_reference_code', 'action_type', 'loading_methods', 'scheduling_type', 'communicated_timeslot', 'note']



class SennderAvroMatchingTransportofferActions(AvroBaseModel):
    loadings: typing.List[SennderAvroMatchingTransportofferAction]
    unloadings: typing.List[SennderAvroMatchingTransportofferAction]



class SennderAvroMatchingLoad(AvroBaseModel):
    """
    Load specifications
    """
    load_id: uuid.UUID
    time_slot: SennderAvroMatchingTransportofferTimeSlot
    reference: str
    cargo_specifications: SennderAvroMatchingTransportOfferLoadDetail
    load_loading_requirements: SennderAvroMatchingTransportofferLoadingRequirements
    actions: SennderAvroMatchingTransportofferActions
    quantity: typing.Optional[str] = None



class SennderAvroMatchingPayload(AvroBaseModel):
    transportoffer_route: SennderAvroMatchingTransportOfferRoute
    financials: SennderAvroMatchingFinancials
    loading_requirements: SennderAvroMatchingTransportofferLoadingRequirements
    facilities: typing.List[SennderAvroMatchingFacility]
    loads: typing.List[SennderAvroMatchingLoad]
    active: bool = Field(metadata={'doc': 'indicates if the TransportOffer is active and matchable'})
    modified: datetime.datetime = Field(metadata={'doc': 'The timestamp when the entity was modified.'})
    shipment_ids: typing.List[uuid.UUID] = Field(default_factory=list,metadata={'doc': "Example: ['4e5415cd-5e78-4110-a651-85954b0e4142', ... ]"})
    allowed_subsidiaries: typing.List[types.Int32] = Field(default_factory=lambda: [1, 3],metadata={'doc': 'Only carriers, belonging to these subsidiaries, can deliver this transport offer'})
    external_id: typing.Optional[str] = None

    class Meta:
        field_order = ['active', 'modified', 'shipment_ids', 'transportoffer_route', 'financials', 'loading_requirements', 'facilities', 'loads', 'allowed_subsidiaries', 'external_id']



class SennderAvroMatchingMatchingTransportOfferKey(AvroBaseModel):
    entity_id: uuid.UUID = Field(metadata={'doc': 'UUID of the transport offer'})
    entity_name: str = Field(metadata={'doc': 'Should be only transport_offer'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})



class SennderAvroMatchingMatchingTransportOfferValue(AvroBaseModel):
    meta: SennderAvroMatchingMetadata
    data: SennderAvroMatchingPayload
