from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing
import uuid


TRANSPORT_PLAN_TOPIC="planning.ev.transport_plan.3"


class SennderAvroTransportPlanningSpan(AvroBaseModel):
    """
    Represents a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.
    """
    span_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.'}, default=None)



class SennderAvroTransportPlanningTracing(AvroBaseModel):
    """
    Allows distributed tracing across services with tools like Datadog.
    """
    span: typing.Optional[SennderAvroTransportPlanningSpan]
    trace_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies an operation that happens across services. It should be passed in between services.'}, default=None)

    class Meta:
        field_order = ['trace_id', 'span']



class SennderAvroTransportPlanningEventMeta(AvroBaseModel):
    tracing: typing.Optional[SennderAvroTransportPlanningTracing]
    event_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the event sent (UUID).'})
    created: datetime.datetime = Field(metadata={'doc': 'Indicates when the message was created.'})
    tenant: str = Field(metadata={'doc': "Tenant without suffix, e.g. 'sennder'."})
    source: str = Field(metadata={'doc': 'Identifier of the service publishing the message.'})
    transaction_id: typing.Optional[uuid.UUID] = Field(metadata={'doc': 'Identifier to track and correlate distributed transaction calls triggered by updates with global impact.'}, default=None)

    class Meta:
        field_order = ['event_id', 'created', 'tenant', 'source', 'tracing', 'transaction_id']



class SennderAvroTransportPlanningTruck(AvroBaseModel):
    id: uuid.UUID
    license_plate: str



class SennderAvroTransportPlanningTrailer(AvroBaseModel):
    id: uuid.UUID
    license_plate: str



class SennderAvroTransportPlanningDriver(AvroBaseModel):
    id: uuid.UUID
    name: typing.Optional[str] = None
    phone_number: typing.Optional[str] = None



class SennderAvroTransportPlanningShipmentAssetsPlan(AvroBaseModel):
    truck: typing.Optional[SennderAvroTransportPlanningTruck] = Field(metadata={'doc': 'Planned Truck for the shipment'}, default=None)
    trailer: typing.Optional[SennderAvroTransportPlanningTrailer] = Field(metadata={'doc': 'Planned Trailer for the shipment'}, default=None)
    driver: typing.Optional[SennderAvroTransportPlanningDriver] = Field(metadata={'doc': 'Planned Driver for the shipment'}, default=None)



class SennderAvroTransportPlanningTimeSlot(AvroBaseModel):
    start: datetime.datetime
    end: datetime.datetime



class SennderAvroTransportPlanningFacilityStop(AvroBaseModel):
    stop_id: uuid.UUID
    facility_id: uuid.UUID
    action_type: str
    booked: typing.Optional[SennderAvroTransportPlanningTimeSlot] = None
    communicated: typing.Optional[SennderAvroTransportPlanningTimeSlot] = None



class SennderAvroTransportPlanningTransportPlan(AvroBaseModel):
    assets: SennderAvroTransportPlanningShipmentAssetsPlan
    stops: typing.List[SennderAvroTransportPlanningFacilityStop]
    shipment_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the shipment (UUID).'})

    class Meta:
        field_order = ['shipment_id', 'assets', 'stops']



class SennderAvroTransportPlanningEventTransportPlan(AvroBaseModel):
    entity: SennderAvroTransportPlanningTransportPlan = Field(metadata={'doc': 'Transport Plan Entity'})
    modified: datetime.datetime = Field(metadata={'doc': 'The timestamp when the transport plan was modified. It enables maintaining the message order on the consumer side'})
    action: typing.Optional[str] = Field(metadata={'doc': 'Optional, Type of the action'}, default=None)
    version: typing.Optional[uuid.UUID] = Field(metadata={'doc': 'Optional, Version of the entity'}, default=None)



class SennderAvroTransportPlanningTransportPlanKey(AvroBaseModel):
    entity_id: uuid.UUID = Field(metadata={'doc': 'Shipment UUID'})
    entity_name: str = Field(metadata={'doc': 'Name of the entity'})
    tenant: str = Field(metadata={'doc': 'Tenant text field'})



class SennderAvroTransportPlanningTransportPlanValue(AvroBaseModel):
    meta: SennderAvroTransportPlanningEventMeta
    data: SennderAvroTransportPlanningEventTransportPlan
