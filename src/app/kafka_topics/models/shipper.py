from dataclasses_avroschema import types
from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing


SHIPPER_TOPIC="sps.ev.shipper.1"


class SennderAvroPortalMetadata(AvroBaseModel):
    shipper_id: str = Field(metadata={'doc': 'uuid shipper identifier (UUID4)'})
    tenant: str = Field(metadata={'doc': 'tenant name'})
    type: str = Field(metadata={'doc': 'Action type'})
    modified: datetime.datetime = Field(metadata={'doc': 'the timestamp when the event was created'})
    version: typing.Optional[types.Int32] = Field(metadata={'doc': 'If the entity has versioning, we want to allow sending it down the stream.'}, default=None)
    trace_id: typing.Optional[str] = Field(metadata={'doc': 'Identifier to allow events distributed tracing across services.'}, default=None)



class SennderAvroPortalVAT(AvroBaseModel):
    """
    VAT (Value-Added Tax) Information
    """
    country_code: typing.Optional[str] = Field(metadata={'doc': 'VAT country code (as used by VIES service)'}, default=None)
    number: typing.Optional[str] = Field(metadata={'doc': 'Value-added tax identification number'}, default=None)



class SennderAvroPortalPayload(AvroBaseModel):
    company_name: typing.Optional[str]
    primary_contact_number: typing.Optional[str]
    primary_contact_comments: typing.Optional[str]
    invoicing_legal_entity: typing.Optional[str]
    account_status: typing.Optional[str]
    account_source: typing.Optional[str]
    mothership_id: typing.Optional[types.Int32]
    salesforce_account_id: typing.Optional[str]
    early_shipper_access: typing.Optional[bool]
    vat: SennderAvroPortalVAT
    last_modified_by: typing.Optional[str]
    accounting_id: typing.Optional[str]
    sub_contracting_policy: typing.Optional[str]
    id: str = Field(metadata={'doc': 'Shipper id'})
    created_at: datetime.datetime = Field(metadata={'doc': 'Date when object was first created'})
    updated_at: datetime.datetime = Field(metadata={'doc': 'Date when object was last updated'})

    class Meta:
        field_order = ['id', 'created_at', 'updated_at', 'company_name', 'primary_contact_number', 'primary_contact_comments', 'invoicing_legal_entity', 'account_status', 'account_source', 'mothership_id', 'salesforce_account_id', 'early_shipper_access', 'vat', 'last_modified_by', 'accounting_id', 'sub_contracting_policy']



class SennderAvroPortalShipperEventKey(AvroBaseModel):
    shipper_id: str = Field(metadata={'doc': 'uuid shipper identifier (UUID4)'})
    tenant: str = Field(metadata={'doc': 'tenant name'})



class SennderAvroPortalShipperEventValue(AvroBaseModel):
    meta: SennderAvroPortalMetadata
    data: SennderAvroPortalPayload
