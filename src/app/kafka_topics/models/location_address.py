from dataclasses_avroschema import types
from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing


LOCATION_ADDRESS_TOPIC="location.ev.address-changed.2"


class IoConfluentConnectAvroConnectDefault(AvroBaseModel):
    id: types.Int32



class SennderAvroLocationConnectDefault(AvroBaseModel):
    id: types.Int32
    tenant: str
    created: datetime.datetime
    modified: datetime.datetime
    country_code: str
    formatted: typing.Optional[str] = None
    postal_code: typing.Optional[str] = None
    locality: typing.Optional[str] = None
    latitude: typing.Optional[float] = None
    longitude: typing.Optional[float] = None
    street_number: typing.Optional[str] = None
    route: typing.Optional[str] = None
    state: typing.Optional[str] = None
    state_code: typing.Optional[str] = None
    timezone: typing.Optional[str] = None
    external_id: typing.Optional[str] = Field(metadata={'doc': 'The id of the external domain model from where this instance was created. In facility domain, this will be the id of the facility address'}, default=None)

    class Meta:
        field_order = ['id', 'tenant', 'created', 'modified', 'formatted', 'country_code', 'postal_code', 'locality', 'latitude', 'longitude', 'street_number', 'route', 'state', 'state_code', 'timezone', 'external_id']
