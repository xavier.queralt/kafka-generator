from dataclasses_avroschema import types
from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import enum
import typing
import uuid


TRANSPORT_OFFER_MATCH_TOPIC="matching.ev.transport-offer-match.3"


class SennderAvroMatchingTransportofferSpan(AvroBaseModel):
    """
    Represents a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.
    """
    span_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.'}, default=None)



class SennderAvroMatchingTransportofferTracing(AvroBaseModel):
    """
    Allows distributed tracing across services with tools like Datadog.
    """
    span: typing.Optional[SennderAvroMatchingTransportofferSpan]
    trace_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies an operation that happens across services. It should be passed in between services.'}, default=None)

    class Meta:
        field_order = ['trace_id', 'span']



class SennderAvroMatchingMetadata(AvroBaseModel):
    created: datetime.datetime
    tracing: typing.Optional[SennderAvroMatchingTransportofferTracing]
    event_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the event sent (UUID).'})
    type: str = Field(metadata={'doc': 'Event type (created | updated)'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})
    source: str = Field(metadata={'doc': "Should always be 'matching'"})
    transaction_id: uuid.UUID = Field(metadata={'doc': 'Identifier to track and correlate distributed transaction calls triggered by updates with global impact.'})
    shipment_ids: typing.List[str] = Field(default_factory=list,metadata={'doc': "Example: ['4e5415cd-5e78-4110-a651-85954b0e4142', ... ]"})
    order_ids: typing.List[int] = Field(default_factory=list,metadata={'doc': 'Example: [100501, 100504]'})

    class Meta:
        field_order = ['event_id', 'created', 'type', 'tenant', 'source', 'transaction_id', 'shipment_ids', 'order_ids', 'tracing']


class SennderAvroMatchingMatchingFlowType(enum.Enum):
    BROKERAGE = "BROKERAGE"
    CHARTERING = "CHARTERING"


class SennderAvroMatchingMatchingState(enum.Enum):
    IN_PROGRESS = "IN_PROGRESS"
    CHARTER_MATCHED = "CHARTER_MATCHED"
    COMPLETED = "COMPLETED"
    CANCELLED = "CANCELLED"



class SennderAvroMatchingCreatorRecord(AvroBaseModel):
    id: str = Field(metadata={'doc': 'The unique identifier for the user who initiated the matching procedure'})
    user_type: str = Field(metadata={'doc': 'The type of the user, who created the match (SYSTEM, CARRIER, SHIPPER)'})



class SennderAvroMatchingCarrierRecord(AvroBaseModel):
    id: uuid.UUID = Field(metadata={'doc': 'Carrier unique identifier - Carrier Profile Service'})
    contact_id: uuid.UUID = Field(metadata={'doc': 'Carrier contact unique identifier - Carrier Profile Service'})



class SennderAvroMatchingCharteringOfficeRecord(AvroBaseModel):
    id: uuid.UUID = Field(metadata={'doc': 'Chartering Office UUID - Chartering Offices service'})



class SennderAvroMatchingTransportofferRecord(AvroBaseModel):
    id: str = Field(metadata={'doc': 'Transport offer unique identifier.'})
    version: types.Int32 = Field(metadata={'doc': 'Transport offer version.'})



class SennderAvroMatchingTransportofferMonetaryAmount(AvroBaseModel):
    """
    Monetary amount expressed in a specific currency.
    """
    number: str
    currency: str = Field(metadata={'doc': 'ISO 4217 code'})



class SennderAvroMatchingSurcharge(AvroBaseModel):
    """
    Surcharges emitted by the shipper, not carrier or brokerage.
    """
    amount: SennderAvroMatchingTransportofferMonetaryAmount
    note: str
    type: str = Field(metadata={'doc': 'mandatory enum indicating the type of the surcharge'})

    class Meta:
        field_order = ['type', 'amount', 'note']



class SennderAvroMatchingFinancials(AvroBaseModel):
    """
    The basic financial price and charges a shipper provides. In the future we will need to re-visit this as base_prices often come from the contract instead.
    """
    cost: SennderAvroMatchingTransportofferMonetaryAmount
    surcharges: typing.List[SennderAvroMatchingSurcharge]
    note: str



class SennderAvroMatchingPayload(AvroBaseModel):
    creator: SennderAvroMatchingCreatorRecord
    transportoffer: SennderAvroMatchingTransportofferRecord
    financials: SennderAvroMatchingFinancials
    created: datetime.datetime = Field(metadata={'doc': "Timestamp of the record's creating time."})
    modified: datetime.datetime = Field(metadata={'doc': 'The timestamp when the entity was modified.'})
    matching_flow: SennderAvroMatchingMatchingFlowType = Field(metadata={'doc': 'Matching flow type (brokerage or chartering)'})
    match_source: str = Field(metadata={'doc': 'Supported values are: CARRIER_BID|CM_BID|CARRIER_ACCEPT_NOW|CM_ACCEPT_NOW|DIRECT|MANUAL_ASSIGNMENT|AUTOMATIC_ASSIGNMENT|CHARTERING_MANUAL_ASSIGNMENT|CHARTERING_AUTO_ASSIGNMENT|CHARTERING_BID'})
    matching_entity_id: typing.Optional[str] = Field(metadata={'doc': 'Entity that triggered matching process, e.g. a bid'}, default=None)
    state: SennderAvroMatchingMatchingState = Field(metadata={'doc': 'Current match state'})
    carrier: typing.Optional[SennderAvroMatchingCarrierRecord] = None
    chartering_office: typing.Optional[SennderAvroMatchingCharteringOfficeRecord] = None

    class Meta:
        field_order = ['created', 'modified', 'matching_flow', 'match_source', 'matching_entity_id', 'state', 'creator', 'carrier', 'chartering_office', 'transportoffer', 'financials']



class SennderAvroMatchingMatchingTransportOfferMatchKey(AvroBaseModel):
    entity_id: uuid.UUID = Field(metadata={'doc': 'UUID of the transport offer match'})
    entity_name: str = Field(metadata={'doc': 'Should be only transport_offer_match'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})



class SennderAvroMatchingMatchingTransportOfferMatchValue(AvroBaseModel):
    meta: SennderAvroMatchingMetadata
    data: SennderAvroMatchingPayload
