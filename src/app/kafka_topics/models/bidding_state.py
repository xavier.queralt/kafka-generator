from dataclasses_avroschema import types
from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing
import uuid


BIDDING_STATE_TOPIC="matching.ev.transport-offer-bidding-state.0"


class SennderAvroMatchingTransportofferSpan(AvroBaseModel):
    """
    Represents a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.
    """
    span_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.'}, default=None)



class SennderAvroMatchingTransportofferTracing(AvroBaseModel):
    """
    Allows distributed tracing across services with tools like Datadog.
    """
    span: typing.Optional[SennderAvroMatchingTransportofferSpan]
    trace_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies an operation that happens across services. It should be passed in between services.'}, default=None)

    class Meta:
        field_order = ['trace_id', 'span']



class SennderAvroMatchingMetadata(AvroBaseModel):
    created: datetime.datetime
    tracing: typing.Optional[SennderAvroMatchingTransportofferTracing]
    event_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the event sent (UUID).'})
    type: str = Field(metadata={'doc': 'Event type (created | updated)'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})
    transaction_id: uuid.UUID = Field(metadata={'doc': 'Identifier to track and correlate distributed transaction calls triggered by updates with global impact.'})

    class Meta:
        field_order = ['event_id', 'created', 'type', 'tenant', 'transaction_id', 'tracing']



class SennderAvroMatchingBid(AvroBaseModel):
    id: types.Int32 = Field(metadata={'doc': 'Bid unique identifier.'})
    order_id: types.Int32 = Field(metadata={'doc': "Order's ID."})
    order_id_for_staff: str = Field(metadata={'doc': "Order's ID for staff."})
    creator_id: types.Int32 = Field(metadata={'doc': "Mothership's user_id of a person who created the bid."})
    carrier_id: types.Int32 = Field(metadata={'doc': "Carrier Company's ID for which the bid was created."})
    carrier_uuid: uuid.UUID = Field(metadata={'doc': "Carrier Company's UUID for which the bid was created."})
    carrier_contact_id: types.Int32 = Field(metadata={'doc': "Carrier Contact's ID that is associated with the bid."})
    carrier_contact_uuid: uuid.UUID = Field(metadata={'doc': "Carrier Contact's UUID that is associated with the bid."})
    state: str = Field(metadata={'doc': "Bid's state (CREATED, ACCEPTED, REJECTED, CANCELLED, WITHDRAWN)."})
    price: types.Int32 = Field(metadata={'doc': "Bid's amount."})
    suggested_price: typing.Optional[types.Int32] = Field(metadata={'doc': "Order's suggested price at the moment of the bid's creation."}, default=None)
    responder_id: typing.Optional[types.Int32] = Field(metadata={'doc': "Mothership's user_id of a person who changed the bid's state."}, default=None)
    notes: typing.Optional[str] = Field(metadata={'doc': 'Notes for the bid.'}, default=None)
    request_source: str = Field(metadata={'doc': 'Platform where the bid was created (OCTOPUS, ORCAS).'})
    rejection_reason: typing.Optional[str] = Field(metadata={'doc': "Bid's rejection reason"}, default=None)
    rejection_note: typing.Optional[str] = Field(metadata={'doc': "Bid's rejection note"}, default=None)
    cancel_reason: typing.Optional[str] = Field(metadata={'doc': "Bid's cancellation reason"}, default=None)
    cancel_note: typing.Optional[str] = Field(metadata={'doc': "Bid's cancellation note"}, default=None)
    created_on: datetime.datetime = Field(metadata={'doc': "Bid's created timestamp."})
    is_accept_now: typing.Optional[bool] = Field(metadata={'doc': 'Whether the bid was created through the Accept Now flow'})
    creator_type: str = Field(metadata={'doc': 'Type of user who created the bid (one of CARRIER, OPS).'})



class SennderAvroMatchingBiddingState(AvroBaseModel):
    state: typing.List[SennderAvroMatchingBid] = Field(metadata={'doc': 'List of active bids'})



class SennderAvroMatchingPayload(AvroBaseModel):
    entity: SennderAvroMatchingBiddingState
    modified: datetime.datetime = Field(metadata={'doc': 'The timestamp when the entity was modified.'})
    action: str = Field(metadata={'doc': 'Type of action (eg. BID_CREATED, BID_UPDATED)'})

    class Meta:
        field_order = ['modified', 'action', 'entity']



class SennderAvroMatchingMatchingTransportOfferBidKey(AvroBaseModel):
    entity_id: uuid.UUID = Field(metadata={'doc': 'UUID of the transport offer'})
    entity_name: str = Field(metadata={'doc': 'Should be only transport_offer'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})



class SennderAvroMatchingMatchingTransportOfferBidValue(AvroBaseModel):
    meta: SennderAvroMatchingMetadata
    data: SennderAvroMatchingPayload
