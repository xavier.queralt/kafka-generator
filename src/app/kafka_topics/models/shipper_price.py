from dataclasses_avroschema import types
from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing
import uuid


SHIPPER_PRICE_TOPIC="settlement.ev.shipper-price.0"


class EventTracing(AvroBaseModel):
    """
    Object to allow distributed tracing across services like Datadog
    """
    trace_id: str = Field(metadata={'doc': 'Identifier to allow distributed tracing across services.'})



class Metadata(AvroBaseModel):
    tracing: EventTracing
    created: datetime.datetime = Field(metadata={'doc': 'The timestamp when the command was created.'})
    event_id: str = Field(metadata={'doc': 'Unique id for each event'})
    source: str = Field(metadata={'doc': 'The service identifier publishing the message.'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant.'})

    class Meta:
        field_order = ['created', 'event_id', 'source', 'tenant', 'tracing']



class SennderAvroSettlementAmount(AvroBaseModel):
    """
    Amount with currency
    """
    value: str
    currency: str



class ChargeCorrection(AvroBaseModel):
    id: uuid.UUID = Field(metadata={'doc': 'Unique correction UUID'})
    amount: SennderAvroSettlementAmount = Field(metadata={'doc': 'Correction amount'})
    comment: str = Field(metadata={'doc': 'Correction comment'})
    created: datetime.datetime = Field(metadata={'doc': 'When the correction was created'})



class Charge(AvroBaseModel):
    corrections: typing.List[ChargeCorrection]
    id: uuid.UUID = Field(metadata={'doc': 'Unique charge UUID'})
    category: str = Field(metadata={'doc': 'Charge category'})
    comment: str = Field(metadata={'doc': 'Charge comment'})
    initial_amount: SennderAvroSettlementAmount = Field(metadata={'doc': 'Charge initial amount'})
    created: datetime.datetime = Field(metadata={'doc': 'When the charge was created'})

    class Meta:
        field_order = ['id', 'category', 'comment', 'initial_amount', 'created', 'corrections']



class Payload(AvroBaseModel):
    charges: typing.List[Charge]
    shipper_price_id: uuid.UUID = Field(metadata={'doc': 'Unique shipper price UUID'})
    action: str = Field(metadata={'doc': 'create / update'})
    mothership_order_id: typing.Optional[types.Int32] = Field(metadata={'doc': 'Mothership order ID'}, default=None)
    mothership_shipper_id: typing.Optional[types.Int32] = Field(metadata={'doc': 'Mothership shipper ID'}, default=None)
    version: types.Int32 = Field(metadata={'doc': 'Shipper price version'})
    state: str = Field(metadata={'doc': 'Shipper price state'})
    total_base_price: SennderAvroSettlementAmount = Field(metadata={'doc': 'Total base price amount'})
    total_extra_charges: SennderAvroSettlementAmount = Field(metadata={'doc': 'Total extra charges amount'})
    external_id: str = Field(metadata={'doc': 'Order id for staff'})
    shipment_id: typing.Optional[uuid.UUID] = None
    closed_at: typing.Optional[datetime.datetime] = None
    new_charge_item_id: typing.Optional[uuid.UUID] = None

    class Meta:
        field_order = ['shipper_price_id', 'action', 'mothership_order_id', 'mothership_shipper_id', 'version', 'state', 'total_base_price', 'total_extra_charges', 'charges', 'external_id', 'shipment_id', 'closed_at', 'new_charge_item_id']



class IoConfluentConnectAvroConnectDefault(AvroBaseModel):
    entity_id: uuid.UUID = Field(metadata={'doc': 'Unique entity id'})
    entity_name: str = Field(metadata={'doc': 'Name of the domain entity'})
    tenant: str = Field(metadata={'doc': 'String identifying the tenant'})



class ConnectDefault(AvroBaseModel):
    meta: Metadata
    data: Payload
