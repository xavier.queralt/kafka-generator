from dataclasses_avroschema.avrodantic import AvroBaseModel
from pydantic import Field
import datetime
import typing
import uuid


PLANNING_TRANSPORT_PLAN_TOPIC="transport_plan.ev.transport_plan.1"


class SennderAvroTransportPlanTransportPlanSpan(AvroBaseModel):
    """
    Represents a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.
    """
    span_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.'}, default=None)



class SennderAvroTransportPlanTransportPlanTracing(AvroBaseModel):
    """
    Allows distributed tracing across services with tools like Datadog.
    """
    span: typing.Optional[SennderAvroTransportPlanTransportPlanSpan]
    trace_id: typing.Optional[str] = Field(metadata={'doc': 'Identifies an operation that happens across services. It should be passed in between services.'}, default=None)

    class Meta:
        field_order = ['trace_id', 'span']



class SennderAvroTransportPlanTransportPlanEventMeta(AvroBaseModel):
    tracing: typing.Optional[SennderAvroTransportPlanTransportPlanTracing]
    event_id: uuid.UUID = Field(metadata={'doc': 'Unique identifier of the event sent (UUID).'})
    created: datetime.datetime = Field(metadata={'doc': 'Indicates when the message was created.'})
    tenant: str = Field(metadata={'doc': "Tenant without suffix, e.g. 'sennder'."})
    source: str = Field(metadata={'doc': 'Identifier of the service publishing the message.'})
    transaction_id: typing.Optional[str] = Field(metadata={'doc': 'Identifier to track and correlate distributed transaction calls triggered by updates with global impact.'}, default=None)

    class Meta:
        field_order = ['event_id', 'created', 'tenant', 'source', 'tracing', 'transaction_id']



class SennderAvroTransportPlanTransportPlanTruck(AvroBaseModel):
    id: uuid.UUID
    license_plate: str



class SennderAvroTransportPlanTransportPlanTrailer(AvroBaseModel):
    id: uuid.UUID
    license_plate: str



class SennderAvroTransportPlanTransportPlanDriver(AvroBaseModel):
    id: uuid.UUID
    name: typing.Optional[str] = None
    phone_number: typing.Optional[str] = None



class SennderAvroTransportPlanTransportPlanTransportPlanAssets(AvroBaseModel):
    truck: typing.Optional[SennderAvroTransportPlanTransportPlanTruck] = Field(metadata={'doc': 'Planned Truck for the shipment'}, default=None)
    trailer: typing.Optional[SennderAvroTransportPlanTransportPlanTrailer] = Field(metadata={'doc': 'Planned Trailer for the shipment'}, default=None)
    driver: typing.Optional[SennderAvroTransportPlanTransportPlanDriver] = Field(metadata={'doc': 'Planned Driver for the shipment'}, default=None)



class SennderAvroTransportPlanTransportPlanStopTimeSlot(AvroBaseModel):
    start: datetime.datetime
    end: typing.Optional[datetime.datetime] = None



class SennderAvroTransportPlanTransportPlanTransportPlanStop(AvroBaseModel):
    """
    The stop populated from the facility plan.
    """
    facility_id: uuid.UUID
    action_type: str
    stop_id: uuid.UUID = Field(metadata={'doc': 'The ID of the stop populated from the facility plan.'})
    booked: typing.Optional[SennderAvroTransportPlanTransportPlanStopTimeSlot] = None
    communicated: typing.Optional[SennderAvroTransportPlanTransportPlanStopTimeSlot] = None

    class Meta:
        field_order = ['stop_id', 'facility_id', 'action_type', 'booked', 'communicated']



class SennderAvroTransportPlanTransportPlanTransportPlan(AvroBaseModel):
    state: str
    assets: SennderAvroTransportPlanTransportPlanTransportPlanAssets
    stops: typing.List[SennderAvroTransportPlanTransportPlanTransportPlanStop]
    shipment_id: uuid.UUID = Field(metadata={'doc': 'Specifies an ID of the shipment.'})
    transport_offer_id: uuid.UUID = Field(metadata={'doc': 'Specifies an ID of the transport offer.'})

    class Meta:
        field_order = ['shipment_id', 'state', 'transport_offer_id', 'assets', 'stops']



class SennderAvroTransportPlanTransportPlanEventDataTransportPlan(AvroBaseModel):
    """
    Transport Plan event payload.
    """
    entity: SennderAvroTransportPlanTransportPlanTransportPlan = Field(metadata={'doc': 'Transport Plan Entity'})
    modified: datetime.datetime = Field(metadata={'doc': 'The timestamp when the asset allocation entity was modified or created. It enables maintaining the message order on the consumer side.'})



class SennderAvroTransportPlanTransportPlanTransportPlanChangedKey(AvroBaseModel):
    entity_id: uuid.UUID = Field(metadata={'doc': 'Specifies the Transport Plan ID.'})
    entity_name: str = Field(metadata={'doc': 'Name of the entity.'})
    tenant: str = Field(metadata={'doc': 'Tenant text field'})



class SennderAvroTransportPlanTransportPlanTransportPlanChangedValue(AvroBaseModel):
    meta: SennderAvroTransportPlanTransportPlanEventMeta
    data: SennderAvroTransportPlanTransportPlanEventDataTransportPlan
