import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.transport_offer_match import TRANSPORT_OFFER_MATCH_TOPIC, SennderAvroMatchingMatchingTransportOfferMatchKey, SennderAvroMatchingMatchingTransportOfferMatchValue, SennderAvroMatchingPayload
from app.libs.utils import custom_standardize_method


def build_message(**data) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroMatchingMatchingTransportOfferMatchKey.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = SennderAvroMatchingMatchingTransportOfferMatchValue.fake(
        data=SennderAvroMatchingPayload.fake(**data)
    ).asdict(custom_standardize_method)

    return TRANSPORT_OFFER_MATCH_TOPIC, KafkaMessage(key=key, value=value)
