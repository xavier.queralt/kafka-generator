import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.transport_plan import TRANSPORT_PLAN_TOPIC, SennderAvroTransportPlanningEventTransportPlan, SennderAvroTransportPlanningFacilityStop, SennderAvroTransportPlanningTimeSlot, SennderAvroTransportPlanningTransportPlan, SennderAvroTransportPlanningTransportPlanKey, SennderAvroTransportPlanningTransportPlanValue
from app.libs.utils import custom_standardize_method


def build_message(facility_ids=[], **data) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroTransportPlanningTransportPlanKey.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = SennderAvroTransportPlanningTransportPlanValue.fake(
        data=SennderAvroTransportPlanningEventTransportPlan.fake(
            entity=SennderAvroTransportPlanningTransportPlan.fake(
                stops=[SennderAvroTransportPlanningFacilityStop.fake(
                    facility_id=fid,
                    communicated=SennderAvroTransportPlanningTimeSlot.fake()
                ) for fid in facility_ids],
                **data
            )
        )
    ).asdict(custom_standardize_method)

    return TRANSPORT_PLAN_TOPIC, KafkaMessage(key=key, value=value)
