import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.shipper_price import ConnectDefault, IoConfluentConnectAvroConnectDefault, Payload, SHIPPER_PRICE_TOPIC
from app.libs.utils import custom_standardize_method


def build_message(**data) -> typing.Tuple[str, KafkaMessage]:
    key = IoConfluentConnectAvroConnectDefault.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = ConnectDefault.fake(
        data=Payload.fake(**data)
    ).asdict(custom_standardize_method)

    return SHIPPER_PRICE_TOPIC, KafkaMessage(key=key, value=value)
