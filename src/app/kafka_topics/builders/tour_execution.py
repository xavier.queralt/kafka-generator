import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.tour_execution import TOUR_EXECUTION_TOPIC, SennderAvroTransitTourExecutionStatusEventData, SennderAvroTransitTourExecutionStatusEventValue, SennderAvroTransitTourExecutionStatusEventKey, SennderAvroTransitTourExecutionStatusEntity
from app.libs.utils import custom_standardize_method


def build_message(**data) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroTransitTourExecutionStatusEventKey.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = SennderAvroTransitTourExecutionStatusEventValue.fake(
        data=SennderAvroTransitTourExecutionStatusEventData.fake(
            entity=SennderAvroTransitTourExecutionStatusEntity.fake(**data)
        )
    ).asdict(custom_standardize_method)

    return TOUR_EXECUTION_TOPIC, KafkaMessage(key=key, value=value)
