import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.shipment import Data, ShipmentValue, ShipmentKey, Shipment, SHIPMENT_TOPIC
from app.libs.utils import custom_standardize_method


def build_message(**data) -> typing.Tuple[str, KafkaMessage]:
    key = ShipmentKey.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = ShipmentValue.fake(
        data=Data.fake(
            entity=Shipment.fake(**data)
        )
    ).asdict(custom_standardize_method)

    return SHIPMENT_TOPIC, KafkaMessage(key=key, value=value)
