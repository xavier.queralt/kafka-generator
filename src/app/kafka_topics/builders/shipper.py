import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.shipper import SennderAvroPortalShipperEventValue, SennderAvroPortalShipperEventKey, SennderAvroPortalPayload, SHIPPER_TOPIC
from app.libs.utils import custom_standardize_method


def build_message(**data) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroPortalShipperEventKey.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = SennderAvroPortalShipperEventValue.fake(
        data=SennderAvroPortalPayload.fake(id=key["shipper_id"], **data)
    ).asdict(custom_standardize_method)

    return SHIPPER_TOPIC, KafkaMessage(key=key, value=value)
