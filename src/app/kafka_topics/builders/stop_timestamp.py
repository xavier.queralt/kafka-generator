import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.stop_timestamp import STOP_TIMESTAMP_TOPIC, SennderAvroMatchingPayload, SennderAvroMatchingSingleViewTransitTourStopTimestampKey, SennderAvroMatchingSingleViewTransitTourStopTimestampValue, SennderAvroMatchingTourExecutionStopTimestamps
from app.libs.utils import custom_standardize_method


def build_message(stop_id, **data) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroMatchingSingleViewTransitTourStopTimestampKey.fake(
        tenant="sennder",
        entity_id=stop_id,
    ).asdict(custom_standardize_method)
    value = SennderAvroMatchingSingleViewTransitTourStopTimestampValue.fake(
        data=SennderAvroMatchingPayload.fake(
            entity=SennderAvroMatchingTourExecutionStopTimestamps.fake(
                id=stop_id,
                **data
            )
        )
    ).asdict(custom_standardize_method)

    return STOP_TIMESTAMP_TOPIC, KafkaMessage(key=key, value=value)
