import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.shipment_draft import SHIPMENT_DRAFT_TOPIC, Data, ShipmentDraftValue, ShipmentKey, Shipment
from app.libs.utils import custom_standardize_method


def build_message(**data) -> typing.Tuple[str, KafkaMessage]:
    key = ShipmentKey.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = ShipmentDraftValue.fake(
        data=Data.fake(
            entity=Shipment.fake(**data)
        )
    ).asdict(custom_standardize_method)

    return SHIPMENT_DRAFT_TOPIC, KafkaMessage(key=key, value=value)
