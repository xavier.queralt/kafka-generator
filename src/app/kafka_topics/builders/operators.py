import typing

from app.kafka_topics.models.operators import SennderAvroCommunicationOperatorsModifiedRecordsKey, SennderAvroOrasOperator, SennderAvroOrasOperatorsModifiedRecordsValue, SennderAvroOrasPayload, OPERATORS_TOPIC
from app.libs.utils import custom_standardize_method
from app.libs.kafka import KafkaMessage


OPERATORS_TOPIC="oras.ev.operators-modified.1"


def build_message(entity_id: str, operator_ids: typing.List[str], roles: typing.List[str]) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroCommunicationOperatorsModifiedRecordsKey.fake(
        tenant="sennder", entity_id=entity_id
    ).asdict(custom_standardize_method)
    value = SennderAvroOrasOperatorsModifiedRecordsValue.fake(
        data=SennderAvroOrasPayload.fake(
            operators=[
                SennderAvroOrasOperator.fake(operator_id=id, role=role) for id, role in zip(operator_ids, roles)
            ]
        )
    ).asdict(custom_standardize_method)

    return OPERATORS_TOPIC, KafkaMessage(key=key, value=value)
