import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.carrier_profile import CARRIER_PROFILE_TOPIC, SennderAvroCarrierProfileCarrier, SennderAvroCarrierProfileEventDataCarrier, SennderAvroCarrierProfileEventKeyCarrier, SennderAvroCarrierProfileEventValueCarrier
from app.libs.utils import custom_standardize_method


def build_message(**data) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroCarrierProfileEventKeyCarrier.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = SennderAvroCarrierProfileEventValueCarrier.fake(
        data=SennderAvroCarrierProfileEventDataCarrier.fake(
            entity=SennderAvroCarrierProfileCarrier.fake(**data)
        )
    ).asdict(custom_standardize_method)

    return CARRIER_PROFILE_TOPIC, KafkaMessage(key=key, value=value)
