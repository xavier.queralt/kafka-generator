import typing
from app.kafka_topics.models.facility_profile import FACILITY_PROFILE_TOPIC, SennderAvroFacilityFacilityProfileKey, SennderAvroFacilityProfileEventDataProfile, SennderAvroFacilityProfileFacilityProfileValue, SennderAvroFacilityProfileProfile
from app.libs.kafka import KafkaMessage

from app.libs.utils import custom_standardize_method


def build_message(**data) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroFacilityFacilityProfileKey.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = SennderAvroFacilityProfileFacilityProfileValue.fake(
        data=SennderAvroFacilityProfileEventDataProfile.fake(
            entity=SennderAvroFacilityProfileProfile.fake(
                id=key["entity_id"], tenant=key["tenant"], **data
            )
        )
    ).asdict(custom_standardize_method)

    return FACILITY_PROFILE_TOPIC, KafkaMessage(key=key, value=value)
