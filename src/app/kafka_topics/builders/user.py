import typing
import uuid
from app.kafka_topics.models.user import (
    USER_TOPIC,
    SennderAvroUserManagementInternalUsersKey,
    SennderAvroUserManagementInternalUsersValue,
    SennderAvroUserManagementPayload
)

from app.libs.kafka import KafkaMessage
from app.libs.utils import custom_standardize_method


def build_message(**data) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroUserManagementInternalUsersKey.fake(
        tenant="sennder",
        user_id=str(uuid.uuid4())
    ).asdict(custom_standardize_method)
    value = SennderAvroUserManagementInternalUsersValue.fake(
        data=SennderAvroUserManagementPayload.fake(
            id=key["user_id"], tenant=key["tenant"], **data
        )
    ).asdict(custom_standardize_method)

    return USER_TOPIC, KafkaMessage(key=key, value=value)
