import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.transport_offer import TRANSPORT_OFFER_TOPIC, SennderAvroMatchingMatchingTransportOfferKey, SennderAvroMatchingMatchingTransportOfferValue, SennderAvroMatchingPayload
from app.libs.utils import custom_standardize_method


def build_message(**data) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroMatchingMatchingTransportOfferKey.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = SennderAvroMatchingMatchingTransportOfferValue.fake(
        data=SennderAvroMatchingPayload.fake(**data)
    ).asdict(custom_standardize_method)

    return TRANSPORT_OFFER_TOPIC, KafkaMessage(key=key, value=value)
