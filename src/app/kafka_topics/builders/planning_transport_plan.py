import typing
from app.libs.kafka import KafkaMessage

from app.kafka_topics.models.planning_transport_plan import PLANNING_TRANSPORT_PLAN_TOPIC, SennderAvroTransportPlanTransportPlanTransportPlanChangedValue, SennderAvroTransportPlanTransportPlanEventDataTransportPlan, SennderAvroTransportPlanTransportPlanTransportPlanChangedKey, SennderAvroTransportPlanTransportPlanTransportPlan, SennderAvroTransportPlanTransportPlanTransportPlanStop, SennderAvroTransportPlanTransportPlanStopTimeSlot
from app.libs.utils import custom_standardize_method


def build_message(facility_ids=[], **data) -> typing.Tuple[str, KafkaMessage]:
    key = SennderAvroTransportPlanTransportPlanTransportPlanChangedKey.fake(
        tenant="sennder"
    ).asdict(custom_standardize_method)
    value = SennderAvroTransportPlanTransportPlanTransportPlanChangedValue.fake(
        data=SennderAvroTransportPlanTransportPlanEventDataTransportPlan.fake(
            entity=SennderAvroTransportPlanTransportPlanTransportPlan.fake(
                stops=[SennderAvroTransportPlanTransportPlanTransportPlanStop.fake(
                    facility_id=fid,
                    communicated=SennderAvroTransportPlanTransportPlanStopTimeSlot.fake()
                ) for fid in facility_ids],
                **data
            )
        )
    ).asdict(custom_standardize_method)

    return PLANNING_TRANSPORT_PLAN_TOPIC, KafkaMessage(key=key, value=value)
