{
    "type": "record",
    "name": "sennder.avro.transport.planning.TransportPlanValue",
    "fields": [
        {
            "name": "meta",
            "type": {
                "type": "record",
                "name": "sennder.avro.transport.planning.EventMeta",
                "fields": [
                    {
                        "doc": "Unique identifier of the event sent (UUID).",
                        "name": "event_id",
                        "type": {
                            "logicalType": "uuid",
                            "type": "string"
                        }
                    },
                    {
                        "doc": "Indicates when the message was created.",
                        "name": "created",
                        "type": {
                            "logicalType": "timestamp-millis",
                            "type": "long"
                        }
                    },
                    {
                        "doc": "Tenant without suffix, e.g. 'sennder'.",
                        "name": "tenant",
                        "type": "string"
                    },
                    {
                        "doc": "Identifier of the service publishing the message.",
                        "name": "source",
                        "type": "string"
                    },
                    {
                        "name": "tracing",
                        "type": [
                            "null",
                            {
                                "type": "record",
                                "doc": "Allows distributed tracing across services with tools like Datadog.",
                                "name": "sennder.avro.transport.planning.Tracing",
                                "fields": [
                                    {
                                        "doc": "Identifies an operation that happens across services. It should be passed in between services.",
                                        "default": null,
                                        "name": "trace_id",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    },
                                    {
                                        "name": "span",
                                        "type": [
                                            "null",
                                            {
                                                "type": "record",
                                                "doc": "Represents a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.",
                                                "name": "sennder.avro.transport.planning.Span",
                                                "fields": [
                                                    {
                                                        "doc": "Identifies a single operation with a start time and end time like a database request, outgoing remote request, or a function invocation.",
                                                        "default": null,
                                                        "name": "span_id",
                                                        "type": [
                                                            "null",
                                                            "string"
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "doc": "Identifier to track and correlate distributed transaction calls triggered by updates with global impact.",
                        "default": null,
                        "name": "transaction_id",
                        "type": [
                            "null",
                            {
                                "logicalType": "uuid",
                                "type": "string"
                            }
                        ]
                    }
                ]
            }
        },
        {
            "name": "data",
            "type": {
                "type": "record",
                "name": "sennder.avro.transport.planning.EventTransportPlan",
                "fields": [
                    {
                        "doc": "Transport Plan Entity",
                        "name": "entity",
                        "type": {
                            "type": "record",
                            "name": "sennder.avro.transport.planning.TransportPlan",
                            "fields": [
                                {
                                    "doc": "Unique identifier of the shipment (UUID).",
                                    "name": "shipment_id",
                                    "type": {
                                        "logicalType": "uuid",
                                        "type": "string"
                                    }
                                },
                                {
                                    "name": "assets",
                                    "type": {
                                        "type": "record",
                                        "name": "sennder.avro.transport.planning.ShipmentAssetsPlan",
                                        "fields": [
                                            {
                                                "doc": "Planned Truck for the shipment",
                                                "default": null,
                                                "name": "truck",
                                                "type": [
                                                    "null",
                                                    {
                                                        "type": "record",
                                                        "name": "sennder.avro.transport.planning.Truck",
                                                        "fields": [
                                                            {
                                                                "name": "id",
                                                                "type": {
                                                                    "logicalType": "uuid",
                                                                    "type": "string"
                                                                }
                                                            },
                                                            {
                                                                "name": "license_plate",
                                                                "type": "string"
                                                            }
                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                "doc": "Planned Trailer for the shipment",
                                                "default": null,
                                                "name": "trailer",
                                                "type": [
                                                    "null",
                                                    {
                                                        "type": "record",
                                                        "name": "sennder.avro.transport.planning.Trailer",
                                                        "fields": [
                                                            {
                                                                "name": "id",
                                                                "type": {
                                                                    "logicalType": "uuid",
                                                                    "type": "string"
                                                                }
                                                            },
                                                            {
                                                                "name": "license_plate",
                                                                "type": "string"
                                                            }
                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                "doc": "Planned Driver for the shipment",
                                                "default": null,
                                                "name": "driver",
                                                "type": [
                                                    "null",
                                                    {
                                                        "type": "record",
                                                        "name": "sennder.avro.transport.planning.Driver",
                                                        "fields": [
                                                            {
                                                                "name": "id",
                                                                "type": {
                                                                    "logicalType": "uuid",
                                                                    "type": "string"
                                                                }
                                                            },
                                                            {
                                                                "default": null,
                                                                "name": "name",
                                                                "type": [
                                                                    "null",
                                                                    "string"
                                                                ]
                                                            },
                                                            {
                                                                "default": null,
                                                                "name": "phone_number",
                                                                "type": [
                                                                    "null",
                                                                    "string"
                                                                ]
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                },
                                {
                                    "name": "stops",
                                    "type": {
                                        "type": "array",
                                        "items": {
                                            "type": "record",
                                            "name": "sennder.avro.transport.planning.FacilityStop",
                                            "fields": [
                                                {
                                                    "name": "stop_id",
                                                    "type": {
                                                        "logicalType": "uuid",
                                                        "type": "string"
                                                    }
                                                },
                                                {
                                                    "name": "facility_id",
                                                    "type": {
                                                        "logicalType": "uuid",
                                                        "type": "string"
                                                    }
                                                },
                                                {
                                                    "name": "action_type",
                                                    "type": "string"
                                                },
                                                {
                                                    "default": null,
                                                    "name": "booked",
                                                    "type": [
                                                        "null",
                                                        {
                                                            "type": "record",
                                                            "name": "sennder.avro.transport.planning.TimeSlot",
                                                            "fields": [
                                                                {
                                                                    "name": "start",
                                                                    "type": [
                                                                        "string",
                                                                        {
                                                                            "logicalType": "timestamp-millis",
                                                                            "type": "long"
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    "default": null,
                                                                    "name": "end",
                                                                    "type": [
                                                                        "null",
                                                                        "string",
                                                                        {
                                                                            "logicalType": "timestamp-millis",
                                                                            "type": "long"
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "default": null,
                                                    "name": "communicated",
                                                    "type": [
                                                        "null",
                                                        "sennder.avro.transport.planning.TimeSlot"
                                                    ]
                                                }
                                            ]
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    {
                        "doc": "The timestamp when the transport plan was modified. It enables maintaining the message order on the consumer side",
                        "name": "modified",
                        "type": {
                            "logicalType": "timestamp-millis",
                            "type": "long"
                        }
                    },
                    {
                        "doc": "Optional, Type of the action",
                        "default": null,
                        "name": "action",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "Optional, Version of the entity",
                        "default": null,
                        "name": "version",
                        "type": [
                            "null",
                            {
                                "logicalType": "uuid",
                                "type": "string"
                            }
                        ]
                    }
                ]
            }
        }
    ]
}