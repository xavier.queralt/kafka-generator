import datetime
import enum
import typing
from dataclasses_avroschema.utils import is_pydantic_model


def custom_standardize_method(value: typing.Any) -> typing.Any:
    if isinstance(value, dict):
        return {k: custom_standardize_method(v) for k, v in value.items()}
    elif isinstance(value, list):
        return [custom_standardize_method(v) for v in value]
    elif isinstance(value, tuple):
        return tuple(custom_standardize_method(v) for v in value)
    elif issubclass(type(value), enum.Enum):
        return value.value
    elif is_pydantic_model(type(value)):
        return custom_standardize_method(value.asdict())
    elif isinstance(value, datetime.datetime):
        return int(value.timestamp()) * 1000
    return value
