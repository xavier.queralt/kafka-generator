import typing as t

import aiokafka
from pydantic import BaseModel
from schema_registry.client import SchemaRegistryClient
from schema_registry.serializers import AvroMessageSerializer


class KafkaMessage(BaseModel):
    key: t.Dict[str, t.Any]
    value: t.Dict[str, t.Any]


class GenericKafkaProducer:
    def __init__(self):
        self.schema_registry = SchemaRegistryClient("http://127.0.0.1:8081")  # CONFIGURATION.kafka.schema_registry_url)
        self.__producers: t.Dict[str, aiokafka.AIOKafkaProducer] = {}
    
    async def producer(self, topic_name: str) -> aiokafka.AIOKafkaProducer:
        if topic_name in self.__producers:
            return self.__producers[topic_name]

        order_value_schema_id = self.schema_registry.get_schema(f"{topic_name}-value").schema_id
        order_key_schema_id = self.schema_registry.get_schema(f"{topic_name}-key").schema_id
        serializer = AvroMessageSerializer(self.schema_registry)

        try:
            self.__producers[topic_name] = aiokafka.AIOKafkaProducer(
                key_serializer=lambda key: serializer.encode_record_with_schema_id(
                    order_key_schema_id, key
                ),
                value_serializer=lambda key: serializer.encode_record_with_schema_id(
                    order_value_schema_id, key
                ),
                bootstrap_servers="127.0.0.1:29092",  #CONFIGURATION.kafka.broker_url,
                sasl_mechanism="PLAIN",
                security_protocol="PLAINTEXT",
                sasl_plain_password="",
                sasl_plain_username="",
            )
            await self.__producers[topic_name].start()

            return self.__producers[topic_name]
        except AttributeError:
            raise Exception(
                f"Cannot find `{topic_name}` in schema registry {self.schema_registry.conf['url']}"
            )

    async def npublish(self, topic_name: str, messages: t.List[KafkaMessage]):
        producer = await self.producer(topic_name)
        batch = producer.create_batch()
        for msg in messages:
            batch.append(key=msg.key, value=msg.value)

        await producer.send_batch(batch=batch, topic=topic_name)

    async def publish(self, topic_name: str, msg: KafkaMessage):
        producer = await self.producer(topic_name)
        await producer.send_and_wait(topic_name, value=msg.value, key=msg.key)
    
    async def stop(self):
        for p in self.__producers.values():
            await p.stop()
