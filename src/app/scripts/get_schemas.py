import json
import os

from typing import Dict, Any
from schema_registry.client import SchemaRegistryClient
from app.libs.settings import TOPICS

SCHEMA_PATH = os.path.join(os.path.dirname(__file__), '../kafka_topics/schemas')

def write_schema(topic_name: str, schema_type: str, schema_dict: Dict[str, Any]) -> Dict[str, Any]:
    with open(f"{SCHEMA_PATH}/{topic_name}-{schema_type}.avsc", "w") as fn:
        fn.write(json.dumps(schema_dict, indent=4))


if __name__ == "__main__":
    schema_registry = SchemaRegistryClient("http://127.0.0.1:8081")  # CONFIGURATION.kafka.schema_registry_url)
    for topic_name in TOPICS.keys():
        print(topic_name)
        for schema_type in ["key", "value"]:
            value_schema_id = schema_registry.get_schema(f"{topic_name}-{schema_type}").schema_id
            schema_dict = schema_registry.get_by_id(value_schema_id).flat_schema

            write_schema(topic_name=topic_name, schema_type=schema_type, schema_dict=schema_dict)