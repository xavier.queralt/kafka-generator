import json
import os

from typing import Dict, Any
from app.libs.settings import TOPICS
from dataclasses_avroschema import ModelGenerator, BaseClassEnum

from app.scripts.get_schemas import SCHEMA_PATH


MODULE_PATH = os.path.join(os.path.dirname(__file__), '../kafka_topics/models')


def generate_module(
    topic_name: str, module_name: str, schema_key: Dict[str, Any], schema_value: Dict[str, Any]
) ->  str:
    model_generator = ModelGenerator(
        base_class=BaseClassEnum.AVRO_DANTIC_MODEL.value,
        extras=[
            f"\n\n{module_name.upper()}_TOPIC=\"{topic_name}\""
        ]
    )

    return model_generator.render_module(schemas=[schema_key, schema_value])


def read_schema(topic_name: str, schema_type: str) -> Dict[str, Any]:
    with open(f"{SCHEMA_PATH}/{topic_name}-{schema_type}.avsc") as fn:
        return json.loads(fn.read())


if __name__ == "__main__":
    for topic_name, module_name in TOPICS.items():
        print(topic_name)    
        result = generate_module(
            topic_name=topic_name,
            module_name=module_name,
            schema_key=read_schema(topic_name, "key"),
            schema_value=read_schema(topic_name, "value")
        )

        with open(f"{MODULE_PATH}/{module_name}.py", "w") as fn:
            fn.write(result)
